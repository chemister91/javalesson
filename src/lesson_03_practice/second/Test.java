package lesson_03_practice.second;

import lesson_01.ru.test.Child;
import lesson_03_practice.first.Parent;
import lesson_03_practice.first.Provider;

/**
 * Created by arty on 01.12.2018.
 */
public class Test {
    public static void main(String s[])
    {
        Provider pr = new Provider();
        Parent p = pr.getValue();
        System.out.println(p.getClass().getName());
         //(Child)p; //- приведет к ошибке компиляции!
    }
}
