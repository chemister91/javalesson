package lesson_04.tryWithResources;

public class SwingException extends Exception {
    public SwingException(String message) {
        super(message);
    }
}
