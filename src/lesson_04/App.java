package lesson_04;

/**
 * Created by arty on 04.12.2018.
 */
public class App {
    public static void main(String[] args) {
        int x = (int)1.0;
        System.out.println(x);
        System.out.println();

        short y = (short)1921222;// Stored as 20678
        System.out.println(y);
        System.out.println();

        int z = (int)9l;
        System.out.println(z);
        System.out.println();

        long t = 192301398193810323L;
        System.out.println(t);
        System.out.println();
    }
}
