package lesson_04;

/**
 * Created by arty on 04.12.2018.
 */
public class Puzzlers {
    public static void main(String[] args) {
        int y = 4;
        double x = 3 + 2 * --y;
        System.out.println(x);
        System.out.println(y);

        //_________________________

        System.out.print("9 / 3 = " + 9 / 3);
        System.out.println();
        System.out.print("9 % 3 = " + 9 % 3);
        System.out.println();
        System.out.print("10 / 3 = " + 10 / 3);
        System.out.println();
        System.out.print("10 % 3 = " + 10 % 3);
        System.out.println();
        System.out.print("11 / 3 = " + 11 / 3);
        System.out.println();
        System.out.print("11 % 3 = " + 11 % 3);
        System.out.println();
        System.out.print("12 / 3 = " + 12 / 3);
        System.out.println();
        System.out.print("12 % 3 = " + 12 % 3);
    }
}
