package lesson_04.exceptions;

public class MyArithmeticException extends ArithmeticException {
    public MyArithmeticException(String s) {
        super(s);
    }
}
