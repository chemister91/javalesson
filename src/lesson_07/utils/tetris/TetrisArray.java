package lesson_07.utils.tetris;

import java.awt.Color;
import java.awt.Graphics;

public class TetrisArray {

    public final static int CELL_HEIGHT = 24;
    public final static int CELL_WIDTH = 24;

    /**
     * Play field.
     * Values:
     *      0 - empty cell
     *      1 - figure
     *      2 - brick
     */
    private int[][] array;
    private int lastI;
    private int lastJ;

    private int countFullLines = 0;
    private boolean gameIsOver = false;

    public TetrisArray() {
        array = new int[6][8];
        lastI = array.length - 1;
        lastJ = array[lastI].length - 1;
    }

    public int getGameResult() {
        return countFullLines;
    }

    public boolean isGameOver() {
        return gameIsOver;
    }

    public boolean isCorrentIndices(int i, int j) {
        boolean correctI = i >= 0 && i < array.length;
        boolean correctJ = j >= 0 && j < array[0].length;
        return correctI && correctJ;
    }

    private boolean canAddFigure() {
        return array[0][3] == 0;
    }

    public void addFigure() {
        if (canAddFigure()) {
            array[0][3] = 1;
        } else {
            gameIsOver = true;
        }
    }

    public void addBrick(int i, int j) {
        if (array[i][j] == 0) {
            array[i][j] = 2;
        }
    }

    public void deleteBrick(int i, int j) {
        if (array[i][j] == 2) {
            array[i][j] = 0;
        }
    }

    private void moveToLeft() {
        for (int i = 0; i <= lastI; i++) {
            for (int j = 1; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    array[i][j - 1] = 1;
                    array[i][j] = 0;
                }
            }
        }
    }

    private void moveToRight() {
        for (int i = 0; i < lastI; i++) {
            for (int j = lastJ - 1; j >= 0; j--) {
                if (array[i][j] == 1) {
                    array[i][j + 1] = 1;
                    array[i][j] = 0;
                }
            }
        }
    }

    private void moveDown() {
        for (int i = lastI - 1; i >= 0; i--) {
            for (int j = 0; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    array[i + 1][j] = 1;
                    array[i][j] = 0;
                }
            }
        }
    }

    private boolean bottomLineIsFull() {
        for (int j = 0; j <= lastJ; j++) {
            if (array[lastI][j] != 2) {
                return false;
            }
        }
        return true;
    }

    private void deleteBottomLine() {
        for (int i = lastI - 1; i >= 0; i--) {
            for (int j = 0; j <= lastJ; j++) {
                array[i + 1][j] = array[i][j];
            }
        }
        for (int j = 0; j <= lastJ; j++) {
            array[0][j] = 0;
        }
    }

    private boolean canMoveDown() {
        for (int i = 0; i < lastI; i++) {
            for (int j = 0; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    if (array[i + 1][j] == 2) {
                        return false;
                    }
                }
            }
        }
        for (int j = 0; j <= lastJ; j++) {
            if (array[lastI][j] == 1) {
                return false;
            }
        }
        return true;
    }

    private boolean canMoveToLeft() {
        for (int i = 0; i <= lastI; i++) {
            for (int j = 1; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    if (array[i][j - 1] == 2) {
                        return false;
                    }
                }
            }
        }
        for (int i = 0; i <= lastI; i++) {
            if (array[i][0] == 1) {
                return false;
            }
        }
        return true;
    }

    private boolean canMoveToRight() {
        for (int i = 0; i <= lastI; i++) {
            for (int j = 0; j < lastJ; j++) {
                if (array[i][j] == 1) {
                    if (array[i][j + 1] == 2) {
                        return false;
                    }
                }
            }
        }
        for (int i = 0; i <= lastI; i++) {
            if (array[i][lastJ] == 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Convert figures to bricks. Delete bottom line if it is full.
     */
    private void fastenFigure() {
        for (int i = 0; i <= lastI; i++) {
            for (int j = 0; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    array[i][j] = 2;
                }
            }
        }
        if (bottomLineIsFull()) {
            deleteBottomLine();
            countFullLines++;
        }
    }

    public boolean figureExist() {
        for (int i = 0; i <= lastI; i++) {
            for (int j = 0; j <= lastJ; j++) {
                if (array[i][j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public void toLeft() {
        if (!figureExist()) {
            return;
        }
        if (canMoveToLeft()) {
            moveToLeft();
        } else {
            fastenFigure();
        }
    }

    public void toRight() {
        if (!figureExist()) {
            return;
        }
        if (canMoveToRight()) {
            moveToRight();
        } else {
            fastenFigure();
        }
    }

    public void fallToBottom() {
        if (!figureExist()) {
            return;
        }
        while (canMoveDown()) {
            moveDown();
        }
        fastenFigure();
    }

    public void fallOneLine() {
        if (!figureExist()) {
            return;
        }
        if (canMoveDown()) {
            moveDown();
        } else {
            fastenFigure();
        }
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i <= lastI; i++) {
            for (int j = 0; j <= lastJ; j++) {
                str = str + array[i][j] + " ";
            }
            str = str + "\n";
        }
        str = str + "\n";
        str = str + "countFullLines=" + countFullLines + "\n";
        if (gameIsOver) {
            str = str + "GAME IS OVER!!!" + "\n";
        }
        return str;
    }

    private Color getColor(int number) {
        Color[] colors = { Color.GRAY, Color.BLUE, Color.RED };
        return colors[number];
    }

    public void drawArray(Graphics g, int width, int height) {
        for (int i = 0; i < array.length; i++) {
            int top = i * CELL_HEIGHT;
            for (int j = 0; j < array[i].length; j++) {
                int left = j * CELL_WIDTH;
                g.setColor(getColor(array[i][j]));
                g.fillRect(left, top, CELL_WIDTH, CELL_HEIGHT);
            }
        }
    }
}
