package lesson_07.utils.tetris;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class TetrisPanel extends JPanel implements MouseListener {

    private static final long serialVersionUID = 7452266272458810116L;

    TetrisArray arr;

    public TetrisPanel(TetrisArray array) {
        arr = array;
        addMouseListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        arr.drawArray(g, this.getWidth(), this.getHeight());
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        int i = me.getY() / TetrisArray.CELL_HEIGHT;
        int j = me.getX() / TetrisArray.CELL_WIDTH;
        if (arr.isCorrentIndices(i, j)) {
            int cntClicks = me.getClickCount();
            if (cntClicks == 2) {
                arr.addBrick(i, j);
            } else {
                arr.deleteBrick(i, j);
            }
            repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub
    }
}
