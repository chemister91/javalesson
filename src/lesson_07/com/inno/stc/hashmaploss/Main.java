package lesson_07.com.inno.stc.hashmaploss;

import java.util.HashMap;

/**
 * Created by a.pervushov on 21.11.2017.
 */
public class Main {
    public static void main(String[] args) {

        BadKey badKeyIn = new BadKey(10);
        BadKey badKeySearch = new BadKey(10);

        HashMap<BadKey, String> map = new HashMap<>();
        map.put(badKeyIn, "val");
        badKeyIn.setValue(99999999);

        System.out.println(map.containsKey(badKeySearch));
    }
}
