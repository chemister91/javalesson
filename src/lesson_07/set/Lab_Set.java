package lesson_07.set;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Lab_Set {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (Integer i = 0; i < 15; i++) {
            set.add(i.toString());
        }
        System.out.println(set);

        set.add("100");
        System.out.println(set);

        set.add("100");
        set.add("100");
        System.out.println(set);

        set.remove(5);
        System.out.println(set);
        set.remove("100");
        System.out.println(set);

        System.out.println(set.size());
        System.out.println(set.isEmpty());

        Set<String> donor = new HashSet<>();
        donor.add("a");
        donor.add("b");
        donor.add("c");
        set.addAll(donor);
        System.out.println(set);

        System.out.println(set.contains("100"));
        System.out.println(set.containsAll(donor));

        set.removeAll(donor);
        System.out.println(set);

        set.add("a");
        set.add("c");
        set.retainAll(donor);
        System.out.println(set);

        String[] strings = set.toArray(new String[0]);
        System.out.println(Arrays.toString(strings));

    }
}
