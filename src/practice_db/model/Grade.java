package practice_db.model;

import java.util.Objects;

public class Grade implements Model {
    private int id;
    private Lesson lesson;
    private Student student;
    private double graduation;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Grade setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean isValid() {
        return lesson != null && student != null;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Grade grade = (Grade) object;
        return lesson.equals(grade.lesson) &&
                student.equals(grade.student);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lesson, student);
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", lesson=" + lesson +
                ", student=" + student +
                ", graduation=" + graduation +
                '}';
    }

    public Lesson getLesson() {
        return lesson;
    }

    public Grade setLesson(Lesson lesson) {
        this.lesson = lesson;
        return this;
    }

    public Student getStudent() {
        return student;
    }

    public Grade setStudent(Student student) {
        this.student = student;
        return this;
    }

    public double getGraduation() {
        return graduation;
    }

    public Grade setGraduation(double graduation) {
        this.graduation = graduation;
        return this;
    }
}
