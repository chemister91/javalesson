package practice_db.model;

public interface Model<T> {

    int getId();

    T setId(int id);

    boolean isValid();

    @Override
    String toString();

    @Override
    boolean equals(Object object);

    @Override
    int hashCode();
}
