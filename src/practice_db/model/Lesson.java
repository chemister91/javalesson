package practice_db.model;

import java.util.Date;
import java.util.Objects;

public class Lesson implements Model {
    private int id;
    private Group group;
    private Date lessonDate;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Lesson setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean isValid() {
        return group != null;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Lesson lesson = (Lesson) object;
        return group.equals(lesson.group) &&
                lessonDate.equals(lesson.lessonDate);
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", group=" + group +
                ", lessonDate=" + lessonDate +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, lessonDate);
    }

    public Group getGroup() {
        return group;
    }

    public Lesson setGroup(Group group) {
        this.group = group;
        return this;
    }

    public Date getLessonDate() {
        return lessonDate;
    }

    public Lesson setLessonDate(Date lessonDate) {
        this.lessonDate = lessonDate;
        return this;
    }
}
