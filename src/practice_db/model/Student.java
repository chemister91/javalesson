package practice_db.model;

import java.util.Date;
import java.util.Objects;

public class Student implements Model<Student> {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date dateOfBirth;
    private Group group;
    private City city;

    public Student() {
    }

    public Student(int id, String firstName, String middleName, String lastName, Date dateOfBirth, Group group, City city) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.group = group;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", group=" + group +
                ", city=" + city +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Student student = (Student) object;
        return firstName.equals(student.firstName) &&
                middleName.equals(student.middleName) &&
                lastName.equals(student.lastName) &&
                dateOfBirth.equals(student.dateOfBirth) &&
                city.equals(student.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, dateOfBirth, city);
    }

    @Override
    public boolean isValid() {
        return firstName != null &&
                middleName != null &&
                lastName != null &&
                dateOfBirth != null &&
                city != null;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Student setId(int id) {
        this.id = id;
        return this;
    }

    public Group getGroup() {
        return group;
    }

    public Student setGroup(Group group) {
        this.group = group;
        return this;
    }

    public City getCity() {
        return city;
    }

    public Student setCity(City city) {
        this.city = city;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Student setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Student setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Student setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Student setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }
}
