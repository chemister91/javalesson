package practice_db.services;

import practice_db.dao.*;
import practice_db.model.Group;
import practice_db.model.Lesson;
import practice_db.model.Student;
import practice_db.model.Grade;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TrainingService {
    private StudentDao studentDao;
    private LessonDao lessonDao;
    private GroupDao groupDao;
    private GradeDao gradeDao;

    public TrainingService() {
        studentDao = new StudentDaoImpl();
        lessonDao = new LessonDaoImpl();
        groupDao = new GroupDaoImpl();
        gradeDao = new GradeDaoImpl();
    }

    public TrainingService createStudent(String firstName, String middleName, String lastName, Date dateOfBirth, String groupName, String cityName) {
        studentDao.createStudent(firstName, middleName, lastName, dateOfBirth, groupName, cityName);
        return this;
    }

    public TrainingService updateStudent(int studentId, String firstName, String middleName, String lastName, Date dateOfBirth, int groupId, int cityId) {
        studentDao.updateStudent(studentId, firstName, middleName, lastName, dateOfBirth, groupId, cityId);
        return this;
    }

    public TrainingService deleteStudent(int studentId) {
        studentDao.deleteStudent(studentDao.getStudentById(studentId));
        return this;
    }

    public TrainingService createGroup(String groupName) {
        groupDao.createGroup(groupName);
        return this;
    }

    public TrainingService updateGroup(int groupId, String groupName) {
        groupDao.updateGroup(groupId, groupName);
        return this;
    }

    public TrainingService deleteGroup(int groupId) {
        groupDao.deleteGroup(groupDao.getGroupById(groupId));
        return this;
    }

    public TrainingService createLesson(int groupId, Date lessonDate) {
        lessonDao.createLesson(groupId, lessonDate);
        return this;
    }

    public TrainingService updateLesson(int lessonId, int groupId, Date lessonDate) {
        lessonDao.updateLesson(lessonId, groupId, lessonDate);
        return this;
    }

    public TrainingService deleteLesson(int lessonId) {
        lessonDao.deleteLesson(lessonDao.getLessonById(lessonId));
        return this;
    }

    public TrainingService rateStudent(int studentId, int lessonId, double graduation) {
        gradeDao.rateStudent(studentId, lessonId, graduation);
        return this;
    }

    public TrainingService deleteRate(int gradeId) {
        gradeDao.deleteGrade(gradeId);
        return this;
    }

    public List<String> getAllStudents() {
        return studentDao
                .getAllStudents()
                .stream()
                .map(Student::toString)
                .collect(Collectors.toList());
    }

    public List<String> getStudentsByGroup(Integer groupId) {
        return studentDao
                .getAllStudents()
                .stream()
                .filter(student -> groupId.equals(student.getGroup().getId()))
                .map(Student::toString)
                .collect(Collectors.toList());
    }

    public List<String> getAllGroups() {
        return groupDao
                .getAllGroups()
                .stream()
                .map(Group::toString)
                .collect(Collectors.toList());
    }

    public List<String> getGradeByLesson(Integer lessonId) {
        return gradeDao
                .getAllGrades()
                .stream()
                .filter(grade -> lessonId.equals(grade.getLesson().getId()))
                .map(Grade::toString)
                .collect(Collectors.toList());
    }

    public List<String> getSchedule() {
        return lessonDao
                .getAllLessons()
                .stream()
                .map(Lesson::toString)
                .collect(Collectors.toList());
    }

    public List<String> getScheduleByStudent(Integer studentId) {
        Integer groupId = studentDao
                .getStudentById(studentId)
                .getGroup()
                .getId();
        return lessonDao
                .getAllLessons()
                .stream()
                .filter(lesson -> groupId.equals(lesson.getGroup().getId()))
                .map(Lesson::toString)
                .collect(Collectors.toList());
    }
}
