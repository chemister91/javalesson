package practice_db.dao;

import practice_db.dao.repos.GradeRepo;
import practice_db.dao.repos.LessonRepo;
import practice_db.dao.repos.StudentRepo;
import practice_db.model.Grade;

import java.util.Collection;

public class GradeDaoImpl implements GradeDao {

    @Override
    public Collection<Grade> getAllGrades() {
        return GradeRepo
                .getInstance()
                .getHashMap()
                .values();
    }

    @Override
    public Grade rateStudent(int studentId, int lessonId, double graduation) {
        Grade grade = new Grade();
        grade
                .setLesson(LessonRepo.getById(lessonId))
                .setStudent(StudentRepo.getById(studentId))
                .setGraduation(graduation);
        return GradeRepo.update(grade);
    }

    @Override
    public void deleteGrade(int gradeId) {
        GradeRepo.delete(GradeRepo.getById(gradeId));
    }
}
