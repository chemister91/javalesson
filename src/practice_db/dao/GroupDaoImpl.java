package practice_db.dao;

import practice_db.dao.repos.GroupRepo;
import practice_db.model.Group;

import java.util.Collection;

public class GroupDaoImpl implements GroupDao {

    @Override
    public Collection<Group> getAllGroups() {
        return GroupRepo
                .getInstance()
                .getHashMap()
                .values();
    }

    @Override
    public Group getGroupById(Integer id) {
        return GroupRepo.getById(id);
    }

    @Override
    public Group createGroup(String name) {
        return GroupRepo.addNew(name);
    }

    @Override
    public Group updateGroup(Group group) {
        return GroupRepo.update(group);
    }

    @Override
    public Group updateGroup(int id, String name) {
        Group group = new Group();
        group
                .setId(id)
                .setName(name);
        return updateGroup(group);
    }

    @Override
    public void deleteGroup(Group group) {
        GroupRepo.delete(group);
    }
}
