package practice_db.dao;

import practice_db.model.Lesson;

import java.util.Collection;
import java.util.Date;

public interface LessonDao {

    Collection<Lesson> getAllLessons();

    Lesson getLessonById(Integer id);

    Lesson createLesson(Integer groupId, Date lessonDate);

    Lesson updateLesson(Lesson lesson);

    Lesson updateLesson(int lessonId, int groupId, Date lessonDate);

    void deleteLesson(Lesson lesson);
}
