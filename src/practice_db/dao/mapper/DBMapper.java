package practice_db.dao.mapper;

import practice_db.dao.dto.Dto;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface DBMapper {

    DBMapper getDataById(Dto dtoClass, int keyId);

    DBMapper insertDataIntoDB(Dto dtoClass);

    DBMapper updateDataInDB(Dto dtoClass);

    DBMapper deleteDataFromDB(Dto dtoClass);

    String createGetSqlQuery(Dto dtoClass, String whereClause);

    String createInsertSqlQuery(Dto dtoClass) throws IllegalAccessException;

    String createUpdateSqlQuery(Dto dtoClass, String whereClause) throws IllegalAccessException;

    String createDeleteSqlQuery(Dto dtoClass, String whereClause);

    Boolean mapResultSetToDto(ResultSet resultSet, Dto dtoClass) throws SQLException, IllegalAccessException;
}
