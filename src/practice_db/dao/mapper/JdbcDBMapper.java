package practice_db.dao.mapper;

import practice_db.dao.connection.ConnectionManager;
import practice_db.dao.connection.ConnectionWrapper;
import practice_db.dao.connection.JdbcConnectionManager;
import practice_db.dao.dto.Dto;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class JdbcDBMapper implements DBMapper {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private ConnectionManager connectionManager;
    private static final String COMMA_SEPARATOR = ", ";
    private static final String OPENING_BRACKET = " (";
    private static final String CLOSING_BRACKET = ") ";
    private static final String SINGLE_QUOTE = "'";
    private static final String SELECT_QRY = "SELECT ";
    private static final String WHERE_QRY = " WHERE ";
    private static final String WHERE_ID_QRY = "id = ?";
    private static final String FROM_QRY = " FROM ";
    private static final String DELETE_QRY = "DELETE ";

    public JdbcDBMapper() {
        this.connectionManager = JdbcConnectionManager.getInstance();
    }

    @Override
    public DBMapper getDataById(Dto dtoClass, int keyId) {
        try (ConnectionWrapper connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    createGetSqlQuery(dtoClass, WHERE_ID_QRY))) {
                preparedStatement.setInt(1, keyId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    mapResultSetToDto(resultSet, dtoClass);
                }
            } finally {
                connectionManager.releaseConnection(connection);
            }
        } catch (SQLException | IllegalAccessException e) {
            logger.warning(e.toString());
        }
        return this;
    }

    public List<Dto> getAllData(Supplier<Dto> dto) {
        try (ConnectionWrapper connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    createGetSqlQuery(dto.get(), ""))) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return mapResultSetToDtoList(resultSet, dto);
                }
            } finally {
                connectionManager.releaseConnection(connection);
            }
        } catch (SQLException | IllegalAccessException e) {
            logger.warning(e.toString());
            return Collections.emptyList();
        }
    }

    @Override
    public DBMapper insertDataIntoDB(Dto dtoClass) {
        try (ConnectionWrapper connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    createInsertSqlQuery(dtoClass), Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.executeUpdate();
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        dtoClass.setId(resultSet.getInt(1));
                    }
                }
            } finally {
                connectionManager.releaseConnection(connection);
            }
        } catch (SQLException | IllegalAccessException e) {
            logger.warning(e.toString());
        }
        return this;
    }

    @Override
    public DBMapper updateDataInDB(Dto dtoClass) {
        try (ConnectionWrapper connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    createUpdateSqlQuery(dtoClass, WHERE_ID_QRY))) {
                preparedStatement.setInt(1, dtoClass.getId());
                preparedStatement.executeUpdate();
            } finally {
                connectionManager.releaseConnection(connection);
            }
        } catch (SQLException | IllegalAccessException e) {
            logger.warning(e.toString());
        }
        return this;
    }

    @Override
    public DBMapper deleteDataFromDB(Dto dtoClass) {
        try (ConnectionWrapper connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    createDeleteSqlQuery(dtoClass, WHERE_ID_QRY))) {
                preparedStatement.setInt(1, dtoClass.getId());
                preparedStatement.executeUpdate();
            } finally {
                connectionManager.releaseConnection(connection);
            }
        } catch (SQLException e) {
            logger.warning(e.toString());
        }
        return this;
    }

    /**
     * Метод генерирует select sql запрос для заполнения dto класса
     *
     * @param dtoClass объект класса, по полям которого формируется sql-запрос
     * @return sql запрос
     */
    @Override
    public String createGetSqlQuery(Dto dtoClass, String whereClause) {
        StringBuilder builder = new StringBuilder();
        for (Field field : dtoClass.getClass().getDeclaredFields()) {
            if (builder.length() != 0) {
                builder.append(COMMA_SEPARATOR);
            }
            builder.append(dtoClass.getFieldMapping(field.getName()));
        }
        builder
                .insert(0, SELECT_QRY)
                .append(FROM_QRY)
                .append(dtoClass.getTableMapping());
        if (!whereClause.isEmpty()) {
            builder
                    .append(WHERE_QRY)
                    .append(whereClause);
        }
        return builder.toString();
    }

    /**
     * Метод генерирует insert sql запрос из dto класса
     * Поле id заполняется из БД
     *
     * @param dtoClass объект класса, по полям которого формируется sql-запрос
     * @return sql запрос
     */
    @Override
    public String createInsertSqlQuery(Dto dtoClass) throws IllegalAccessException {
        StringBuilder keys = new StringBuilder();
        StringBuilder values = new StringBuilder();
        StringBuilder builder = new StringBuilder();
        boolean accessible;
        for (Field field : dtoClass.getClass().getDeclaredFields()) {
            if (keys.length() != 0) {
                keys.append(COMMA_SEPARATOR);
                values.append(COMMA_SEPARATOR);
            }
            if ("id".equals(field.getName())) {
                continue;
            }
            keys.append(dtoClass.getFieldMapping(field.getName()));
            accessible = field.isAccessible();
            try {
                field.setAccessible(true);
                values
                        .append(SINGLE_QUOTE)
                        .append(field.get(dtoClass))
                        .append(SINGLE_QUOTE);
            } finally {
                field.setAccessible(accessible);
            }
        }
        builder
                .append("INSERT INTO ")
                .append(dtoClass.getTableMapping())
                .append(OPENING_BRACKET)
                .append(keys)
                .append(CLOSING_BRACKET)
                .append("VALUES")
                .append(OPENING_BRACKET)
                .append(values)
                .append(CLOSING_BRACKET);
        return builder.toString();
    }

    /**
     * Метод генерирует update sql запрос из dto класса
     *
     * @param dtoClass объект класса, по полям которого формируется sql-запрос
     * @return sql запрос
     */
    @Override
    public String createUpdateSqlQuery(Dto dtoClass, String whereClause) throws IllegalAccessException {
        StringBuilder builder = new StringBuilder();
        StringBuilder fieldValues = new StringBuilder();
        boolean accessible;
        for (Field field : dtoClass.getClass().getDeclaredFields()) {
            if (fieldValues.length() != 0) {
                fieldValues.append(COMMA_SEPARATOR);
            }
            if ("id".equals(field.getName())) {
                continue;
            }
            accessible = field.isAccessible();
            try {
                field.setAccessible(true);
                fieldValues
                        .append(dtoClass.getFieldMapping(field.getName()))
                        .append(" = ")
                        .append(SINGLE_QUOTE)
                        .append(field.get(dtoClass))
                        .append(SINGLE_QUOTE);
            } finally {
                field.setAccessible(accessible);
            }
        }
        builder
                .append("UPDATE ")
                .append(dtoClass.getTableMapping())
                .append(" SET ")
                .append(fieldValues);
        if (!whereClause.isEmpty()) {
            builder
                    .append(WHERE_QRY)
                    .append(whereClause);
        }
        return builder.toString();
    }

    /**
     * Метод генерирует delete sql запрос, используя имя dto класса
     *
     * @param dtoClass объект класса, содержимое которого нужно удалить из бд
     * @return sql запрос
     */
    @Override
    public String createDeleteSqlQuery(Dto dtoClass, String whereClause) {
        StringBuilder builder = new StringBuilder();
        builder
                .append(DELETE_QRY)
                .append(FROM_QRY)
                .append(dtoClass.getTableMapping());
        if (!whereClause.isEmpty()) {
            builder
                    .append(WHERE_QRY)
                    .append(whereClause);
        }
        return builder.toString();
    }

    /**
     * Метод мапит результат запроса на поля класса
     * Предполагается, что resultSet получен в результате запроса,
     * сгенерированного в методе createGetSqlQuery
     *
     * @param resultSet результат sql-запроса
     * @param dtoClass  объект класса, который на поля которого мапится resultSet
     * @return this
     * @throws SQLException
     * @throws IllegalAccessException
     */
    @Override
    public Boolean mapResultSetToDto(ResultSet resultSet, Dto dtoClass) throws
            SQLException, IllegalAccessException {
        boolean accessible;
        boolean hasNext = resultSet.next();
        if (hasNext) {
            for (Field field : dtoClass.getClass().getDeclaredFields()) {
                accessible = field.isAccessible();
                try {
                    field.setAccessible(true);
                    field.set(dtoClass, resultSet.getObject(dtoClass.getFieldMapping(field.getName())));
                } finally {
                    field.setAccessible(accessible);
                }
            }
        }
        return hasNext;
    }

    public List<Dto> mapResultSetToDtoList(ResultSet resultSet, Supplier<Dto> dto) throws SQLException, IllegalAccessException {
        List<Dto> dtoList = new ArrayList<>();
        boolean hasNext;
        do {
            Dto dtoClass = dto.get();
            hasNext = mapResultSetToDto(resultSet, dtoClass);
            if (dtoClass.isValid()) {
                dtoList.add(dtoClass);
            }
        } while (hasNext);
        return dtoList;
    }
}
