package practice_db.dao;

import practice_db.dao.repos.GroupRepo;
import practice_db.dao.repos.LessonRepo;
import practice_db.model.Lesson;

import java.util.Collection;
import java.util.Date;

public class LessonDaoImpl implements LessonDao {

    @Override
    public Collection<Lesson> getAllLessons() {
        return LessonRepo
                .getInstance()
                .getHashMap()
                .values();
    }

    @Override
    public Lesson getLessonById(Integer id) {
        return LessonRepo.getById(id);
    }

    @Override
    public Lesson createLesson(Integer groupId, Date lessonDate) {
        Lesson lesson = new Lesson();
        lesson
                .setGroup(GroupRepo.getById(groupId))
                .setLessonDate(lessonDate);
        return LessonRepo.addNew(lesson);
    }

    @Override
    public Lesson updateLesson(Lesson lesson) {
        return LessonRepo.update(lesson);
    }

    @Override
    public Lesson updateLesson(int lessonId, int groupId, Date lessonDate) {
        Lesson lesson = new Lesson();
        lesson
                .setId(lessonId)
                .setLessonDate(lessonDate)
                .setGroup(GroupRepo.getById(groupId));
        return updateLesson(lesson);
    }

    @Override
    public void deleteLesson(Lesson lesson) {
        LessonRepo.delete(lesson);
    }
}
