package practice_db.dao.dto;

import java.util.Date;

public class StudentDto implements Dto {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date dateOfBirth;
    private Integer groupID;
    private Integer cityID;

    @Override
    public String getTableMapping() {
        return "univer.public.students";
    }

    @Override
    public String getFieldMapping(String fieldName) {
        switch (fieldName) {
            case "id":
                return "id";
            case "firstName":
                return "first_name";
            case "middleName":
                return "middle_name";
            case "lastName":
                return "last_name";
            case "dateOfBirth":
                return "date_of_birth";
            case "groupID":
                return "group_id";
            case "cityID":
                return "city_id";
            default:
                throw new IllegalArgumentException("Invalid field name");
        }
    }

    @Override
    public boolean isValid() {
        return firstName != null &&
                middleName != null &&
                lastName != null &&
                dateOfBirth != null;
    }

    public Integer getId() {
        return id;
    }

    public StudentDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public StudentDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public StudentDto setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public StudentDto setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public StudentDto setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public StudentDto setGroupID(Integer groupID) {
        this.groupID = groupID;
        return this;
    }

    public Integer getCityID() {
        return cityID;
    }

    public StudentDto setCityID(Integer cityID) {
        this.cityID = cityID;
        return this;
    }
}
