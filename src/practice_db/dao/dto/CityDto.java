package practice_db.dao.dto;

public class CityDto implements Dto {
    private int id;
    private String name;

    @Override
    public String getTableMapping() {
        return "univer.public.cities";
    }

    @Override
    public String getFieldMapping(String fieldName) {
        switch (fieldName) {
            case "id":
                return "id";
            case "name":
                return "name";
            default:
                throw new IllegalArgumentException("Invalid field name");
        }
    }

    @Override
    public boolean isValid() {
        return name != null;
    }

    public Integer getId() {
        return id;
    }

    public CityDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CityDto setName(String name) {
        this.name = name;
        return this;
    }
}
