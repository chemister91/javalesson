package practice_db.dao.dto;

public interface Dto<T> {
    String getFieldMapping(String fieldName);

    String getTableMapping();

    boolean isValid();

    Integer getId();

    T setId(Integer id);
}
