package practice_db.dao.dto;

public class GroupDto implements Dto {
    private Integer id;
    private String name;

    @Override
    public String getTableMapping() {
        return "univer.public.groups";
    }

    @Override
    public String getFieldMapping(String fieldName) {
        switch (fieldName) {
            case "name":
                return "name";
            case "id":
                return "id";
            default:
                throw new IllegalArgumentException("Invalid field name");
        }
    }

    @Override
    public boolean isValid() {
        return name != null;
    }

    public Integer getId() {
        return id;
    }

    public GroupDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public GroupDto setName(String name) {
        this.name = name;
        return this;
    }
}
