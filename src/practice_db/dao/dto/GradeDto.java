package practice_db.dao.dto;

public class GradeDto implements Dto {
    private int id;
    private int lessonId;
    private int studentId;
    private double graduation;

    @Override
    public String getFieldMapping(String fieldName) {

        switch (fieldName) {
            case "id":
                return "id";
            case "lessonId":
                return "lesson_id";
            case "studentId":
                return "student_id";
            case "graduation":
                return "grade";
            default:
                throw new IllegalArgumentException("Invalid field name");
        }
    }

    @Override
    public String getTableMapping() {
        return "univer.public.grades";
    }

    @Override
    public boolean isValid() {
        return studentId != 0 && lessonId != 0;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public GradeDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public int getLessonId() {
        return lessonId;
    }

    public GradeDto setLessonId(int lessonId) {
        this.lessonId = lessonId;
        return this;
    }

    public int getStudentId() {
        return studentId;
    }

    public GradeDto setStudentId(int studentId) {
        this.studentId = studentId;
        return this;
    }

    public double getGraduation() {
        return graduation;
    }

    public GradeDto setGraduation(double graduation) {
        this.graduation = graduation;
        return this;
    }
}
