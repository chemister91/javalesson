package practice_db.dao.dto;

import java.util.Date;

public class LessonDto implements Dto {
    private int id;
    private int groupId;
    private Date lessonDate;

    @Override
    public String getFieldMapping(String fieldName) {
        switch (fieldName) {
            case "id":
                return "id";
            case "groupId":
                return "group_id";
            case "lessonDate":
                return "date";
            default:
                throw new IllegalArgumentException("Invalid field name");
        }
    }

    @Override
    public String getTableMapping() {
        return "univer.public.lessons";
    }

    @Override
    public boolean isValid() {
        return groupId != 0;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public LessonDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public int getGroupId() {
        return groupId;
    }

    public LessonDto setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    public Date getLessonDate() {
        return lessonDate;
    }

    public LessonDto setLessonDate(Date lessonDate) {
        this.lessonDate = lessonDate;
        return this;
    }
}
