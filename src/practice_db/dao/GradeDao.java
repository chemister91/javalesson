package practice_db.dao;

import practice_db.model.Grade;

import java.util.Collection;

public interface GradeDao {
    Collection<Grade> getAllGrades();

    Grade rateStudent(int studentId, int lessonId, double graduation);

    void deleteGrade(int gradeId);
}
