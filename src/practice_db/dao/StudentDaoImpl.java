package practice_db.dao;

import practice_db.dao.repos.CityRepo;
import practice_db.dao.repos.GroupRepo;
import practice_db.dao.repos.StudentRepo;
import practice_db.model.Student;

import java.util.Collection;
import java.util.Date;

public class StudentDaoImpl implements StudentDao {

    @Override
    public Collection<Student> getAllStudents() {
        return StudentRepo
                .getInstance()
                .getHashMap()
                .values();
    }

    @Override
    public Student getStudentById(Integer id) {
        return StudentRepo.getById(id);
    }

    @Override
    public Student createStudent(String firstName, String middleName, String lastName, Date dateOfBirth, String groupName, String cityName) {
        Student student = new Student();
        student
                .setFirstName(firstName)
                .setMiddleName(middleName)
                .setLastName(lastName)
                .setDateOfBirth(dateOfBirth)
                .setGroup(GroupRepo.addNew(groupName))
                .setCity(CityRepo.addNew(cityName));
        return StudentRepo.addNew(student);
    }

    @Override
    public Student updateStudent(Student student) {
        return StudentRepo.update(student);
    }

    @Override
    public Student updateStudent(int studentId, String firstName, String middleName, String lastName, Date dateOfBirth, int groupId, int cityId) {
        Student student = new Student();
        student
                .setId(studentId)
                .setFirstName(firstName.isEmpty() ? student.getFirstName() : firstName)
                .setMiddleName(middleName.isEmpty() ? student.getMiddleName() : middleName)
                .setLastName(lastName.isEmpty() ? student.getLastName() : lastName)
                .setDateOfBirth(dateOfBirth == null ? student.getDateOfBirth() : dateOfBirth)
                .setGroup(groupId == 0 ? student.getGroup() : GroupRepo.getById(groupId))
                .setCity(cityId == 0 ? student.getCity() : CityRepo.getById(cityId));
        return updateStudent(student);
    }

    @Override
    public void deleteStudent(Student student) {
        StudentRepo.delete(student);
    }
}
