package practice_db.dao;

import practice_db.model.Student;

import java.util.Collection;
import java.util.Date;

public interface StudentDao {
    Collection<Student> getAllStudents();

    Student getStudentById(Integer id);

    Student createStudent(String firstName, String middleName, String lastName, Date dateOfBirth, String groupName, String cityName);

    Student updateStudent(Student student);

    Student updateStudent(int studentId, String firstName, String middleName, String lastName, Date dateOfBirth, int groupId, int cityId);

    void deleteStudent(Student student);
}
