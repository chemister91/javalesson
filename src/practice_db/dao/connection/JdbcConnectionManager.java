package practice_db.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class JdbcConnectionManager implements ConnectionManager {
    private static final JdbcConnectionManager INSTANCE = new JdbcConnectionManager();
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private List<Connection> connectionList;
    private Integer connectionPoolSize;
    private String CONNECTION_URL = "jdbc:postgresql://localhost:5432/univer";
    private String CONNECTION_USER = "postgres";
    private String CONNECTION_PASSWORD = "postgre";

    private JdbcConnectionManager() {
        connectionList = new LinkedList<>();
        connectionPoolSize = 10;
    }

    public static JdbcConnectionManager getInstance() {
        return INSTANCE;
    }

    @Override
    public synchronized ConnectionWrapper getConnection() throws SQLException {
        if (connectionPoolSize > 0) {
            Connection connection = null;
            if (!connectionList.isEmpty()) {
                connection = connectionList.remove(0);
            } else {
                try {
                    Class.forName("org.postgresql.Driver");
                    connection = DriverManager.getConnection(
                            CONNECTION_URL,
                            CONNECTION_USER, CONNECTION_PASSWORD);
                } catch (SQLException | ClassNotFoundException e) {
                    logger.severe(e.toString());
                }
                connectionPoolSize--;
            }
            return new ConnectionWrapper(connection);
        } else {
            throw new SQLException("Connection pool overflow");
        }
    }

    @Override
    public synchronized void releaseConnection(ConnectionWrapper connectionWrapper) {
        if (connectionWrapper != null) {
            connectionList.add(connectionWrapper.getConnection());        }
    }

    public Integer getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(Integer connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }
}
