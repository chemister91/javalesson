package practice_db.dao.connection;

import java.sql.SQLException;

public interface ConnectionManager {
    ConnectionWrapper getConnection() throws SQLException;

    void releaseConnection(ConnectionWrapper connectionWrapper);
}
