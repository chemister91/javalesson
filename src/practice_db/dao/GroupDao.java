package practice_db.dao;

import practice_db.model.Group;

import java.util.Collection;

public interface GroupDao {
    Collection<Group> getAllGroups();

    Group getGroupById(Integer id);

    Group createGroup(String name);

    Group updateGroup(Group group);

    Group updateGroup(int id, String name);

    void deleteGroup(Group group);
}
