package practice_db.dao.repos;

import practice_db.dao.dto.StudentDto;
import practice_db.model.Student;

public class StudentRepo extends Repo<Student, StudentDto> {
    private static final StudentRepo INSTANCE = new StudentRepo();

    public static StudentRepo getInstance() {
        return INSTANCE;
    }

    public static Student getById(Integer id) {
        return INSTANCE.getModel(id);
    }

    public static Student addNew(Student student) {
        return INSTANCE.createModel(student);
    }

    public static Student update(Student student) {
        return INSTANCE.updateModel(student);
    }

    public static void delete(Student student) {
        INSTANCE.deleteModel(student);
    }

    @Override
    Student getNewModel() {
        return new Student();
    }

    @Override
    StudentDto getNewDto() {
        return new StudentDto();
    }

    @Override
    protected Student refreshModelFields(Student student) {
        Student studentFromMap = hashMap.get(student.getId());
        if (modelIsValid(studentFromMap)) {
            studentFromMap
                    .setLastName(student.getLastName())
                    .setMiddleName(student.getMiddleName())
                    .setFirstName(student.getFirstName())
                    .setDateOfBirth(student.getDateOfBirth())
                    .setCity(student.getCity())
                    .setGroup(student.getGroup());
        }
        return studentFromMap;
    }

    @Override
    protected Student mapDtoToModel(StudentDto studentDto, Student student) {
        if (dtoIsValid(studentDto)) {
            student
                    .setId(studentDto.getId())
                    .setFirstName(studentDto.getFirstName())
                    .setMiddleName(studentDto.getMiddleName())
                    .setLastName(studentDto.getLastName())
                    .setDateOfBirth(studentDto.getDateOfBirth())
                    .setGroup(GroupRepo.getById(studentDto.getGroupID()))
                    .setCity(CityRepo.getById(studentDto.getCityID()));
        }
        return student;
    }

    @Override
    protected StudentDto mapModelToDto(StudentDto studentDto, Student student) {
        if (modelIsValid(student)) {
            studentDto
                    .setId(student.getId())
                    .setFirstName(student.getFirstName())
                    .setMiddleName(student.getMiddleName())
                    .setLastName(student.getLastName())
                    .setDateOfBirth(student.getDateOfBirth())
                    .setGroupID(student.getGroup().getId())
                    .setCityID(student.getCity().getId());
        }
        return studentDto;
    }
}
