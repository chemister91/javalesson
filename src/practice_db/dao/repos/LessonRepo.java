package practice_db.dao.repos;

import practice_db.dao.dto.LessonDto;
import practice_db.model.Lesson;

public class LessonRepo extends Repo<Lesson, LessonDto> {
    private static final LessonRepo INSTANCE = new LessonRepo();

    public static LessonRepo getInstance() {
        return INSTANCE;
    }

    public static Lesson getById(Integer id) {
        return INSTANCE.getModel(id);
    }

    public static Lesson addNew(Lesson lesson) {
        return INSTANCE.createModel(lesson);
    }

    public static Lesson update(Lesson lesson) {
        return INSTANCE.updateModel(lesson);
    }

    public static void delete(Lesson lesson) {
        INSTANCE.deleteModel(lesson);
    }

    @Override
    Lesson getNewModel() {
        return new Lesson();
    }

    @Override
    LessonDto getNewDto() {
        return new LessonDto();
    }

    @Override
    Lesson refreshModelFields(Lesson lesson) {
        Lesson lessonFromMap = hashMap.get(lesson.getId());
        if (modelIsValid(lessonFromMap)) {
            lessonFromMap
                    .setGroup(lesson.getGroup())
                    .setLessonDate(lesson.getLessonDate());
        }
        return lessonFromMap;
    }

    @Override
    Lesson mapDtoToModel(LessonDto lessonDto, Lesson lesson) {
        if (lessonDto != null) {
            lesson
                    .setId(lessonDto.getId())
                    .setGroup(GroupRepo.getById(lessonDto.getGroupId()))
                    .setLessonDate(lessonDto.getLessonDate());
        }
        return lesson;
    }

    @Override
    LessonDto mapModelToDto(LessonDto lessonDto, Lesson lesson) {
        if (modelIsValid(lesson)) {
            lessonDto
                    .setId(lesson.getId())
                    .setGroupId(lesson.getGroup().getId())
                    .setLessonDate(lesson.getLessonDate());
        }
        return lessonDto;
    }
}
