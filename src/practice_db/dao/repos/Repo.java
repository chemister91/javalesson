package practice_db.dao.repos;

import practice_db.dao.dto.Dto;
import practice_db.model.Model;
import practice_db.dao.mapper.JdbcDBMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Logger;

public abstract class Repo<M, D> {
    protected Logger logger;
    Map<Integer, M> hashMap;
    private long refreshInterval;
    private long lastRefreshTime;

    Repo() {
        hashMap = new HashMap<>();
        refreshInterval = 10_000L;
        logger = Logger.getLogger(this.getClass().getName());
        getAllModelsFromDB();
    }

    abstract M getNewModel();

    abstract D getNewDto();

    abstract M refreshModelFields(M model);

    abstract M mapDtoToModel(D dto, M model);

    abstract D mapModelToDto(D dto, M model);

    private Supplier<Dto> getNewDto = () -> (Dto) getNewDto();

    M getModel(int id) {
        if (!hashMap.containsKey(id)) {
            hashMap.put(id, getModelFromDB(id));
        } else if (System.currentTimeMillis() - lastRefreshTime > refreshInterval) {
            getModelFromDB(hashMap.get(id));
        }
        return hashMap.get(id);
    }

    M updateModel(M model) {
        return updateModel(model, true);
    }

    void deleteModel(M model) {
        if (hashMap.containsValue(model)) {
            deleteModelFromDB(model, getNewDto());
            hashMap.remove(model);
        }
    }

    M createModel(M model) {
        if (modelIsValid(model)) {
            if (hashMap.containsValue(model)) {
                for (M modelFromMap : hashMap.values()) {
                    if (modelFromMap.equals(model)) {
                        model = modelFromMap;
                    }
                }
            } else if (model != null) {
                insertModelToDB(model);
                hashMap.put(((Model) model).getId(), model);
            }
        }
        return model;
    }

    private M getModelFromDB(int id) {
        return getModelFromDB(getNewModel(), id);
    }

    private M getModelFromDB(M model, int id) {
        return getModelFromDB(model, getNewDto(), id);
    }

    private M getModelFromDB(M model) {
        return getModelFromDB(model, ((Model) model).getId());
    }

    private M getModelFromDB(M model, D dto, int id) {
        logger.info("Get " + model.getClass().getSimpleName() + " from database");
        updateLastRefreshTime();
        JdbcDBMapper dbMapper = new JdbcDBMapper();
        dbMapper.getDataById((Dto) dto, id);
        return mapDtoToModel(dto, model);
    }

    void getAllModelsFromDB() {
        getAllModelsFromDB(getNewModel());
    }

    private void getAllModelsFromDB(M model) {
        logger.info("Get all " + model.getClass().getSimpleName() + " from database");
        updateLastRefreshTime();
        JdbcDBMapper dbMapper = new JdbcDBMapper();
        dbMapper
                .getAllData(getNewDto)
                .forEach(dtoClass ->
                        updateModel(mapDtoToModel((D) dtoClass, getNewModel()), false));
    }

    private M insertModelToDB(M model) {
        return insertModelToDB(model, getNewDto());
    }

    private M insertModelToDB(M model, D dto) {
        if (model != null) {
            logger.info("Insert " + model.getClass().getSimpleName() + " into database");
            updateLastRefreshTime();
            JdbcDBMapper dbMapper = new JdbcDBMapper();
            dbMapper.insertDataIntoDB((Dto) mapModelToDto(dto, model));
            ((Model) model).setId(((Dto) dto).getId());
        }
        return model;
    }

    private M updateModel(M model, boolean updateInDB) {
        if (((Model) model).getId() == 0) {
            model = createModel(model);
        } else {
            if (!hashMap.containsKey(((Model) model).getId())) {
                hashMap.put(((Model) model).getId(), model);
            } else {
                refreshModelFields(model);
            }
            if (updateInDB) {
                updateModelInDB(model);
            }
        }
        return model;
    }

    private M updateModelInDB(M model) {
        return updateModelInDB(model, getNewDto());
    }

    private M updateModelInDB(M model, D dto) {
        if (model != null) {
            logger.info("Update " + model.getClass().getSimpleName() + " in database");
            updateLastRefreshTime();
            JdbcDBMapper dbMapper = new JdbcDBMapper();
            dbMapper.updateDataInDB((Dto) mapModelToDto(dto, model));
        }
        return model;
    }

    private void deleteModelFromDB(M model, D dto) {
        logger.info("Delete " + model.getClass().getSimpleName() + " from database");
        updateLastRefreshTime();
        JdbcDBMapper dbMapper = new JdbcDBMapper();
        dbMapper.deleteDataFromDB((Dto) mapModelToDto(dto, model));
    }

    boolean modelIsValid(M model) {
        return (model != null) && ((Model) model).isValid();
    }

    boolean dtoIsValid(D dto) {
        return (dto != null) && ((Dto) dto).isValid();
    }

    private void updateLastRefreshTime() {
        lastRefreshTime = System.currentTimeMillis();
    }

    public long getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(long refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public Map<Integer, M> getHashMap() {
        return hashMap;
    }
}
