package practice_db.dao.repos;

import practice_db.dao.dto.CityDto;
import practice_db.model.City;

public class CityRepo extends Repo<City, CityDto> {
    private static final CityRepo INSTANCE = new CityRepo();

    public static CityRepo getInstance() {
        return INSTANCE;
    }

    public static City getById(Integer id) {
        return INSTANCE.getModel(id);
    }

    public static City addNew(City city) {
        return INSTANCE.createModel(city);
    }

    public static City addNew(String cityName) {
        return INSTANCE.createModel(new City().setName(cityName));
    }

    public static City update(City city) {
        return INSTANCE.updateModel(city);
    }

    public static void delete(City city) {
        INSTANCE.deleteModel(city);
    }

    @Override
    City getNewModel() {
        return new City();
    }

    @Override
    CityDto getNewDto() {
        return new CityDto();
    }

    @Override
    City refreshModelFields(City city) {
        City cityFromMap = hashMap.get(city.getId());
        if (modelIsValid(cityFromMap)) {
            cityFromMap.setName(city.getName());
        }
        return cityFromMap;
    }

    @Override
    protected City mapDtoToModel(CityDto cityDto, City city) {
        if (cityDto != null) {
            city
                    .setId(cityDto.getId())
                    .setName(cityDto.getName());
        }
        return city;
    }

    @Override
    CityDto mapModelToDto(CityDto cityDto, City city) {
        if (modelIsValid(city)) {
            cityDto
                    .setId(city.getId())
                    .setName(city.getName());
        }
        return cityDto;
    }
}
