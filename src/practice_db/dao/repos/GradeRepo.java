package practice_db.dao.repos;

import practice_db.dao.dto.GradeDto;
import practice_db.model.Grade;

public class GradeRepo extends Repo<Grade, GradeDto> {
    private static final GradeRepo INSTANCE = new GradeRepo();

    public static GradeRepo getInstance() {
        return INSTANCE;
    }

    public static Grade getById(Integer id) {
        return INSTANCE.getModel(id);
    }

    public static Grade update(Grade grade) {
        return INSTANCE.updateModel(grade);
    }

    public static void delete(Grade grade) {
        INSTANCE.deleteModel(grade);
    }

    @Override
    Grade getNewModel() {
        return new Grade();
    }

    @Override
    GradeDto getNewDto() {
        return new GradeDto();
    }

    @Override
    Grade refreshModelFields(Grade grade) {
        Grade gradeFromMap = hashMap.get(grade.getId());
        if (modelIsValid(gradeFromMap)) {
            gradeFromMap
                    .setLesson(grade.getLesson())
                    .setStudent(grade.getStudent())
                    .setGraduation(grade.getGraduation());
        }
        return gradeFromMap;
    }

    @Override
    Grade mapDtoToModel(GradeDto gradeDto, Grade grade) {
        if (gradeDto != null) {
            grade
                    .setId(gradeDto.getId())
                    .setStudent(StudentRepo.getById(gradeDto.getStudentId()))
                    .setLesson(LessonRepo.getById(gradeDto.getLessonId()))
                    .setGraduation(gradeDto.getGraduation());
        }
        return grade;
    }

    @Override
    GradeDto mapModelToDto(GradeDto gradeDto, Grade grade) {
        if (modelIsValid(grade)) {
            gradeDto
                    .setId(grade.getId())
                    .setLessonId(grade.getLesson().getId())
                    .setStudentId(grade.getStudent().getId())
                    .setGraduation(grade.getGraduation());
        }
        return gradeDto;
    }
}
