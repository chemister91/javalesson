package practice_db.dao.repos;

import practice_db.dao.dto.GroupDto;
import practice_db.model.Group;

public class GroupRepo extends Repo<Group, GroupDto> {
    private static final GroupRepo INSTANCE = new GroupRepo();

    public static GroupRepo getInstance() {
        return INSTANCE;
    }

    public static Group getById(Integer id) {
        return INSTANCE.getModel(id);
    }

    public static Group addNew(Group group) {
        return INSTANCE.createModel(group);
    }

    public static Group addNew(String groupName) {
        return INSTANCE.createModel(new Group().setName(groupName));
    }

    public static Group update(Group group) {
        return INSTANCE.updateModel(group);
    }

    public static void delete(Group group) {
        INSTANCE.deleteModel(group);
    }

    @Override
    Group getNewModel() {
        return new Group();
    }

    @Override
    GroupDto getNewDto() {
        return new GroupDto();
    }

    @Override
    Group refreshModelFields(Group group) {
        Group groupFromMap = hashMap.get(group.getId());
        if (modelIsValid(groupFromMap)) {
            groupFromMap
                    .setName(group.getName());
        }
        return groupFromMap;
    }

    @Override
    GroupDto mapModelToDto(GroupDto groupDto, Group group) {
        if (modelIsValid(group)) {
            groupDto
                    .setId(group.getId())
                    .setName(group.getName());
        }
        return groupDto;
    }

    @Override
    protected Group mapDtoToModel(GroupDto groupDto, Group group) {
        if (groupDto != null) {
            group
                    .setId(groupDto.getId())
                    .setName(groupDto.getName());
        }
        return group;
    }
}
