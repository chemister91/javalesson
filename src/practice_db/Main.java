package practice_db;

import practice_db.services.TrainingService;

import java.sql.Date;

public class Main {

    public static void main(String[] args) {
        TrainingService service = new TrainingService();
        service
                .createGroup("Вторая группа")
                .createStudent("Игорь", "Петрович", "Сидоров", Date.valueOf("1951-11-12"), "Первая группа", "Москва")
                .createLesson(1, Date.valueOf("2019-01-15"))
                .createLesson(2, Date.valueOf("2019-04-01"));
    }
}
