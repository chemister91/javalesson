package practice_multithreading;

import com.sun.xml.internal.fastinfoset.CommonResourceBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class MultiThreading {
    private int poolSize = 5;
    private int threadCount = 4;
    List<Thread> threads;

    private static Runnable work = new Runnable() {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void runPoolThread() {
        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);
        for (int i = 0; i < threadCount; i++) {
            Runnable thread;
        }

    }

    public void runThreads() {
        threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            threads.add(new Thread(work));
        }

    }

    public void interruptThread(int number){
        threads.get(number).currentThread().interrupt();
    }

    public void runChronometer(){
        Message message = new Message();
        Thread everySeconds = new Thread(new Chronometer(1000, message, Algorithm.SIMPLE));
        Thread everyFiveSeconds = new Thread(new Chronometer(5000, message, Algorithm.SIMPLE));
        Thread notifier = new Thread(new Notifier(message, Algorithm.SIMPLE));
        notifier.start();
        everySeconds.start();
        everyFiveSeconds.start();
    }

    public void runChronometerUsingReentrantLock(){
        Message message = new Message();
        new Thread(new Chronometer(1000, message, Algorithm.REENTRANTLOCK)).start();
        new Thread(new Chronometer(5000, message, Algorithm.REENTRANTLOCK)).start();
        new Thread(new Notifier(message, Algorithm.REENTRANTLOCK)).start();
    }

    public void deadLock(){
        Message message1 = new Message();
        Message message2 = new Message();
        new Thread(new Chronometer(message1, message2, Algorithm.DEADLOCK)).start();
        new Thread(new Chronometer(message2, message1, Algorithm.DEADLOCK)).start();
    }

}
