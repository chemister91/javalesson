package practice_multithreading;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Message {
    private String msg;
    private ReentrantLock locker;

    public ReentrantLock getLocker() {
        return locker;
    }

    public Condition getCondition() {
        return condition;
    }

    private Condition condition;

    public Message() {
        locker = new ReentrantLock();
        condition = locker.newCondition();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
