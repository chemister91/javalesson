package practice_multithreading;

import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class Notifier implements Runnable {
    private Date startTime;
    private Message monitor;
    private Algorithm algorithm;

    public Notifier(Message monitor, Algorithm algorithm) {
        this.startTime = new Date();
        this.monitor = monitor;
        this.algorithm = algorithm;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()){
            switch (algorithm){
                case REENTRANTLOCK:
                    reentrantLock();
                    break;
                case SIMPLE:
                    simple();
                    break;
            }
        }
    }

    private void simple(){
        try {
            synchronized (monitor) {
                monitor.wait();
                System.out.println(monitor.getMsg() + "Время работы программы:" + (new Date().getTime() - startTime.getTime())/1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void reentrantLock(){
        monitor.getLocker().lock();
        try {
            monitor.getCondition().await();
            System.out.println(monitor.getMsg() + "Время работы программы:" + (new Date().getTime() - startTime.getTime())/1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            monitor.getLocker().unlock();
        }
    }
}
