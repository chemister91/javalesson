package practice_multithreading;

public enum Algorithm {
    SIMPLE,
    REENTRANTLOCK,
    DEADLOCK;
}
