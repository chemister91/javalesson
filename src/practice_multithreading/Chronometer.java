package practice_multithreading;

public class Chronometer implements Runnable {

    private int notifyPeriod;
    private Message monitor;
    private Message monitor1;
    private Message monitor2;
    private Algorithm algorithm;

    public Chronometer(Message monitor1, Message monitor2, Algorithm algorithm) {
        this.monitor1 = monitor1;
        this.monitor2 = monitor2;
        this.algorithm = algorithm;
    }

    public Chronometer(int notifyPeriod, Message monitor, Algorithm algorithm) {
        this.notifyPeriod = notifyPeriod;
        this.monitor = monitor;
        this.algorithm = algorithm;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            switch (algorithm){
                case SIMPLE:
                    simple();
                    break;
                case REENTRANTLOCK:
                    reentrantLock();
                    break;
                case DEADLOCK:
                    deadLock();
                    break;
            }
        }
    }

    private String getMsg(){
        return String.format("%d-секундный поток: ", notifyPeriod / 1000);
    }

    private void simple(){
        try {
            Thread.sleep(notifyPeriod);
            synchronized (monitor) {
                monitor.setMsg(getMsg());
                monitor.notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void reentrantLock(){
        monitor.getLocker().lock();
        try {
            monitor.setMsg(getMsg());
            monitor.getCondition().await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            monitor.getLocker().unlock();
        }
    }

    private void deadLock() {
        synchronized (monitor1){
            System.out.println("Блокировка!");
            synchronized (monitor2){
                System.out.println("Разблокировка!");
            }
        }
    }
}
