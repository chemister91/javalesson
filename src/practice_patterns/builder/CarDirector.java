package practice_patterns.builder;

import practice_patterns.builder.car_features.*;

public class CarDirector {
    public Car constructEconomyCar(CarBuilder builder) {
        return builder
                .setLight(new Light(LightType.HALOGEN))
                .setInterior(new Interior(MaterialType.DERMANTIN))
                .setEngine(new Engine(EngineType.TYPE_1_5_T))
                .setAirConditioner(new AirConditioner(AirConditionerType.NONE))
                .setWheel(new Wheel(17, DiskType.SIMPLE))
                .setMediaSystem(new MediaSystem(MediaSystemType.CASSETTE_PLAYER))
                .build();
    }

    public Car constructComfortCar(CarBuilder builder) {
        return builder
                .setLight(new Light(LightType.LED))
                .setInterior(new Interior(MaterialType.VELOUR))
                .setEngine(new Engine(EngineType.TYPE_2_0))
                .setAirConditioner(new AirConditioner(AirConditionerType.SIMPLE))
                .setWheel(new Wheel(18, DiskType.SIMPLE))
                .setMediaSystem(new MediaSystem(MediaSystemType.FLASH_PLAYER))
                .build();
    }

    public Car constructBusinessCar(CarBuilder builder) {
        return builder
                .setLight(new Light(LightType.XENON))
                .setInterior(new Interior(MaterialType.LEATHER))
                .setEngine(new Engine(EngineType.TYPE_2_4))
                .setAirConditioner(new AirConditioner(AirConditionerType.CLIMATE_CONTROL))
                .setWheel(new Wheel(20, DiskType.ALLOY))
                .setMediaSystem(new MediaSystem(MediaSystemType.SUPER_PLAYER))
                .build();
    }
}
