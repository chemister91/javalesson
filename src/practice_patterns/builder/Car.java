package practice_patterns.builder;

import practice_patterns.builder.car_features.*;

public class Car {
    private Light light;
    private Interior interior;
    private Wheel wheel;
    private Engine engine;
    private AirConditioner airConditioner;
    private MediaSystem mediaSystem;

    public Car(Light light, Interior interior, Wheel wheel, Engine engine, AirConditioner airConditioner, MediaSystem mediaSystem) {
        this.light = light;
        this.interior = interior;
        this.wheel = wheel;
        this.engine = engine;
        this.airConditioner = airConditioner;
        this.mediaSystem = mediaSystem;
    }

    public Car(Car car) {
        if (car != null) {
            this.light = car.light;
            this.interior = car.interior;
            this.wheel = car.wheel;
            this.engine = car.engine;
            this.airConditioner = car.airConditioner;
            this.mediaSystem = car.mediaSystem;
        }
    }

    public Car clone() {
        return new Car(this);
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public Interior getInterior() {
        return interior;
    }

    public void setInterior(Interior interior) {
        this.interior = interior;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public AirConditioner getAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(AirConditioner airConditioner) {
        this.airConditioner = airConditioner;
    }

    public MediaSystem getMediaSystem() {
        return mediaSystem;
    }

    public void setMediaSystem(MediaSystem mediaSystem) {
        this.mediaSystem = mediaSystem;
    }

    @Override
    public String toString() {
        return "Car{" +
                "light=" + light +
                ", interior=" + interior +
                ", wheel=" + wheel +
                ", engine=" + engine +
                ", airConditioner=" + airConditioner +
                ", mediaSystem=" + mediaSystem +
                '}';
    }
}
