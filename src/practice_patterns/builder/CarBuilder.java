package practice_patterns.builder;

import practice_patterns.builder.car_features.*;

public interface CarBuilder {
    CarBuilderImpl setWheel(Wheel wheel);

    CarBuilderImpl setLight(Light light);

    CarBuilderImpl setAirConditioner(AirConditioner airConditioner);

    CarBuilderImpl setEngine(Engine engine);

    CarBuilderImpl setInterior(Interior interior);

    CarBuilderImpl setMediaSystem(MediaSystem mediaSystem);
}
