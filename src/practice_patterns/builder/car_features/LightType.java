package practice_patterns.builder.car_features;

public enum LightType {
    XENON,
    HALOGEN,
    LED;
}
