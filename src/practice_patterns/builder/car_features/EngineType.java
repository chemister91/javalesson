package practice_patterns.builder.car_features;

public enum EngineType {
    TYPE_1_5_T,
    TYPE_2_0,
    TYPE_2_4;

}
