package practice_patterns.builder.car_features;

public class Interior {
    private MaterialType materialType;

    public Interior(MaterialType materialType) {
        this.materialType = materialType;
    }

    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public String toString() {
        return "Interior{" +
                "materialType=" + materialType +
                '}';
    }
}
