package practice_patterns.builder.car_features;

public class Light {
    private LightType lightType;

    public Light(LightType lightType) {
        this.lightType = lightType;
    }

    public LightType getLightType() {
        return lightType;
    }

    @Override
    public String toString() {
        return "Light{" +
                "lightType=" + lightType +
                '}';
    }
}
