package practice_patterns.builder.car_features;

public enum MediaSystemType {
    CASSETTE_PLAYER,
    DISK_PLAYER,
    FLASH_PLAYER,
    GPS_PLAYER,
    SUPER_PLAYER
}
