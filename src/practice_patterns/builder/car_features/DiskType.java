package practice_patterns.builder.car_features;

public enum DiskType {
    SIMPLE,
    ALLOY;
}
