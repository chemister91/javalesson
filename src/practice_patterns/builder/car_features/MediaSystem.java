package practice_patterns.builder.car_features;

public class MediaSystem {
    MediaSystemType mediaSystemType;

    public MediaSystem(MediaSystemType mediaSystemType) {
        this.mediaSystemType = mediaSystemType;
    }

    public MediaSystemType getMediaSystemType() {
        return mediaSystemType;
    }

    @Override
    public String toString() {
        return "MediaSystem{" +
                "mediaSystemType=" + mediaSystemType +
                '}';
    }
}
