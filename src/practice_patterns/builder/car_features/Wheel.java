package practice_patterns.builder.car_features;

public class Wheel {
    private Integer size;
    private DiskType diskType;

    public Wheel(Integer size, DiskType diskType) {
        this.size = size;
        this.diskType = diskType;
    }

    public Integer getSize() {
        return size;
    }

    public DiskType getDiskType() {
        return diskType;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "size=" + size +
                ", diskType=" + diskType +
                '}';
    }
}
