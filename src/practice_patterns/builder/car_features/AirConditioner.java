package practice_patterns.builder.car_features;

public class AirConditioner {
    AirConditionerType airConditionerType;

    public AirConditioner(AirConditionerType airConditionerType) {
        this.airConditionerType = airConditionerType;
    }

    public AirConditionerType getAirConditionerType() {
        return airConditionerType;
    }

    @Override
    public String toString() {
        return "AirConditioner{" +
                "airConditionerType=" + airConditionerType +
                '}';
    }
}
