package practice_patterns.builder.car_features;

public class Engine {
    private EngineType engineType;

    public Engine(EngineType engineType) {
        this.engineType = engineType;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "engineType=" + engineType +
                '}';
    }
}
