package practice_patterns.builder.car_features;

public enum AirConditionerType {
    NONE,
    SIMPLE,
    CLIMATE_CONTROL;
}
