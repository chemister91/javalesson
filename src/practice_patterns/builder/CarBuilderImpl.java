package practice_patterns.builder;

import practice_patterns.builder.car_features.*;

public class CarBuilderImpl implements CarBuilder {
    private Wheel wheel;
    private Light light;
    private AirConditioner airConditioner;
    private Engine engine;
    private Interior interior;
    private MediaSystem mediaSystem;

    @Override
    public CarBuilderImpl setWheel(Wheel wheel) {
        this.wheel = wheel;
        return this;
    }

    @Override
    public CarBuilderImpl setLight(Light light) {
        this.light = light;
        return this;
    }

    @Override
    public CarBuilderImpl setAirConditioner(AirConditioner airConditioner) {
        this.airConditioner = airConditioner;
        return this;
    }

    @Override
    public CarBuilderImpl setEngine(Engine engine) {
        this.engine = engine;
        return this;
    }

    @Override
    public CarBuilderImpl setInterior(Interior interior) {
        this.interior = interior;
        return this;
    }

    @Override
    public CarBuilderImpl setMediaSystem(MediaSystem mediaSystem) {
        this.mediaSystem = mediaSystem;
        return this;
    }

    Car build() {
        return new Car(light, interior, wheel, engine, airConditioner, mediaSystem);
    }
}