package practice_patterns.abstact_factory;

public class OffRoadFactory extends VehicleFactory {
    private static Wheel wheel = new Wheel(WheelType.OFFROAD);

    @Override
    public Bicycle createBicycle() {
        return new BicycleImpl(wheel, 2, 25.1, 11.4);
    }

    @Override
    public Car createCar() {
        return new CarImpl(wheel, 4, 36.7, 15.6);
    }

    @Override
    public Motorbike createMotorBike() {
        return new MotorBikeImpl(wheel, 2, 30.3, 11.2);
    }
}
