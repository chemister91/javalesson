package practice_patterns.abstact_factory;

public interface Car {
    void drive();

    void stop();

    void refuel();
}
