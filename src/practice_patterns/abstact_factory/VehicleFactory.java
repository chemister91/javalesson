package practice_patterns.abstact_factory;

public abstract class VehicleFactory {
    public abstract Bicycle createBicycle();

    public abstract Car createCar();

    public abstract Motorbike createMotorBike();
}
