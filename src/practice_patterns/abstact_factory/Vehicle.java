package practice_patterns.abstact_factory;

public abstract class Vehicle {
    private Wheel wheel;
    private Integer wheelCount;
    private Double clearance;
    private Double suspensionRate;

    public Vehicle(Wheel wheel, Integer wheelCount, Double clearance, Double suspensionRate) {
        this.wheel = wheel;
        this.wheelCount = wheelCount;
        this.clearance = clearance;
        this.suspensionRate = suspensionRate;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Integer getWheelCount() {
        return wheelCount;
    }

    public void setWheelCount(Integer wheelCount) {
        this.wheelCount = wheelCount;
    }

    public Double getClearance() {
        return clearance;
    }

    public void setClearance(Double clearance) {
        this.clearance = clearance;
    }

    public Double getSuspensionRate() {
        return suspensionRate;
    }

    public void setSuspensionRate(Double suspensionRate) {
        this.suspensionRate = suspensionRate;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                "{" +
                "wheel=" + wheel +
                ", wheelCount=" + wheelCount +
                ", clearance=" + clearance +
                ", suspensionRate=" + suspensionRate +
                '}';
    }
}
