package practice_patterns.abstact_factory;

public interface Bicycle {
    void drive();

    void stop();
}
