package practice_patterns.abstact_factory;

public class CarImpl extends Vehicle implements Car {

    public CarImpl(Wheel wheel, Integer wheelCount, Double clearance, Double suspensionRate) {
        super(wheel, wheelCount, clearance, suspensionRate);
    }

    @Override
    public void drive() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void refuel() {

    }
}
