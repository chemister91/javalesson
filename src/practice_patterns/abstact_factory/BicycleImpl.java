package practice_patterns.abstact_factory;

public class BicycleImpl extends Vehicle implements Bicycle {
    public BicycleImpl(Wheel wheel, Integer wheelCount, Double clearance, Double suspensionRate) {
        super(wheel, wheelCount, clearance, suspensionRate);
    }

    @Override
    public void drive() {

    }

    @Override
    public void stop() {

    }
}
