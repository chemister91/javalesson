package practice_patterns.abstact_factory;

public class Wheel {
    private WheelType wheelType;

    public Wheel(WheelType wheelType) {
        this.wheelType = wheelType;
    }

    public WheelType getWheelType() {
        return wheelType;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "wheelType=" + wheelType +
                '}';
    }
}
