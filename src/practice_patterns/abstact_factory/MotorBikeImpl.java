package practice_patterns.abstact_factory;

public class MotorBikeImpl extends Vehicle implements Motorbike {
    public MotorBikeImpl(Wheel wheel, Integer wheelCount, Double clearance, Double suspensionRate) {
        super(wheel, wheelCount, clearance, suspensionRate);
    }

    @Override
    public void drive() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void refuel() {

    }

    @Override
    public void clean() {

    }
}
