package practice_patterns.abstact_factory;

public class SimpleFactory extends VehicleFactory {
    private static Wheel wheel = new Wheel(WheelType.SIMPLE);

    @Override
    public Bicycle createBicycle() {
        return new BicycleImpl(wheel, 2, 11.1, 2.4);
    }

    @Override
    public Car createCar() {
        return new CarImpl(wheel, 4, 20.7, 3.6);
    }

    @Override
    public Motorbike createMotorBike() {
        return new MotorBikeImpl(wheel, 2, 10.3, 4.2);
    }
}
