package practice_patterns.abstact_factory;

public interface Motorbike {
    void drive();

    void stop();

    void refuel();

    void clean();
}
