package practice_patterns.abstact_factory;

public enum WheelType {
    SIMPLE,
    RACING,
    OFFROAD;
}
