package practice_patterns.abstact_factory;

public class RacingFactory extends VehicleFactory {
    private static Wheel wheel = new Wheel(WheelType.RACING);

    @Override
    public Bicycle createBicycle() {
        return new BicycleImpl(wheel, 2, 15.1, 3.4);
    }

    @Override
    public Car createCar() {
        return new CarImpl(wheel, 4, 16.7, 5.6);
    }

    @Override
    public Motorbike createMotorBike() {
        return new MotorBikeImpl(wheel, 2, 10.3, 4.2);
    }
}
