package practice_patterns.factory;

import java.util.Collection;

public abstract class Window {
    private Integer x;
    private Integer y;
    private Integer height;
    private Integer width;
    private Collection<Object> objectCollection;

    public Window(Integer x, Integer y, Integer height, Integer width) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Collection<Object> getObjectCollection() {
        return objectCollection;
    }

    public void setObjectCollection(Collection<Object> objectCollection) {
        this.objectCollection = objectCollection;
    }

    public Window addNewOject(Object object) {
        objectCollection.add(object);
        return this;
    }
}
