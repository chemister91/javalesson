package practice_patterns.factory;

public class Button {
    private Integer x;
    private Integer y;
    private String text;

    public Button(Integer x, Integer y, String text) {
        this.x = x;
        this.y = y;
        this.text = text;
    }
}
