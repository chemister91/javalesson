package practice_patterns.factory;

public class SimpleWindowThread extends MyThread{
    @Override
    public void createFactory() {
        Factory factory = new Factory();
        factory.createWindow("Simple");
    }
}
