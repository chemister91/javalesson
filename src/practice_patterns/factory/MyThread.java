package practice_patterns.factory;

public abstract class MyThread {
    public abstract void createFactory();
}
