package practice_patterns.factory;

public class DialodWindowThread extends MyThread {

    @Override
    public void createFactory() {
        Factory factory = new Factory();
        factory.createWindow("Dialog");
    }
}
