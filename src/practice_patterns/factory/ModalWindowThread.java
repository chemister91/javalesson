package practice_patterns.factory;

public class ModalWindowThread extends MyThread {
    @Override
    public void createFactory() {
        Factory factory = new Factory();
        factory.createWindow("Modal");
    }
}
