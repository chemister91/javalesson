package practice_patterns.factory;

public class Factory {
    public final String BUTTON_CLOSE = "Close";
    public final String BUTTON_MINIMIZE = "Minimize";
    public final String BUTTON_MAXIMIZE = "Maximize";
    public final String BUTTON_OK = "Ok";

    public Window createWindow(String widowsType) {
        if (widowsType.equals("Simple")) {
            return new SimpleWindow("Simple", 100, 200, 300, 400)
                    .addNewOject(new Button(99, 199, BUTTON_CLOSE))
                    .addNewOject(new Button(98, 199, BUTTON_MINIMIZE))
                    .addNewOject(new Button(97, 199, BUTTON_MAXIMIZE));
        } else if (widowsType.equals("Dialog")) {
            return new DialogWindow("Dialog", 200, 300, 400, 500)
                    .addNewOject(new Button(199, 299, BUTTON_OK));
        } else if (widowsType.equals("Modal")) {
            return new ModalWindow(100, 200, 400, 500)
                    .addNewOject(new Button(99, 199, BUTTON_CLOSE));
        } else {
            throw new IllegalArgumentException("invalid parameter"); //
        }
    }
}
