package practice_patterns.factory;

public class SimpleWindow extends Window {
    private String header;

    public SimpleWindow(String header, Integer x, Integer y, Integer height, Integer width) {
        super(x, y, height, width);
        this.header = header;
    }
}
