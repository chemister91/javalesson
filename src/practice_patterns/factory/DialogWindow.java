package practice_patterns.factory;

public class DialogWindow extends Window {
    private String header;

    public DialogWindow(String header, Integer x, Integer y, Integer height, Integer width) {
        super(x, y, height, width);
        this.header = header;
    }
}
