package practice_patterns;

import practice_patterns.abstact_factory.OffRoadFactory;
import practice_patterns.abstact_factory.RacingFactory;
import practice_patterns.abstact_factory.SimpleFactory;
import practice_patterns.abstact_factory.VehicleFactory;
import practice_patterns.builder.CarBuilder;
import practice_patterns.builder.CarBuilderImpl;
import practice_patterns.builder.CarDirector;
import practice_patterns.immutable.ImmutableClass;
import practice_patterns.immutable.MyComplexType;

public class Main {
    public static void main(String[] args) {
        MyComplexType myComplexType = new MyComplexType(9, "nine");
        ImmutableClass immutableClass = new ImmutableClass(11, "eleven", myComplexType);
        myComplexType.setInteger(99);
        myComplexType.setString("none");
        System.out.println(myComplexType);
        System.out.println(immutableClass);

        VehicleFactory vehicleFactory = new SimpleFactory();
        System.out.println(vehicleFactory.createBicycle());
        System.out.println(vehicleFactory.createCar().toString());
        System.out.println(vehicleFactory.createMotorBike());
        System.out.println(new OffRoadFactory().createCar());
        System.out.println(new RacingFactory().createBicycle());

        CarBuilder carBuilder = new CarBuilderImpl();
        CarDirector carDirector = new CarDirector();
        System.out.println(carDirector.constructEconomyCar(carBuilder));
        System.out.println(carDirector.constructComfortCar(carBuilder));
        System.out.println(carDirector.constructBusinessCar(carBuilder));
    }
}
