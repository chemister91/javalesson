package practice_patterns.immutable;

public class ImmutableClass {
    private final Integer integer;
    private final String string;
    private final MyComplexType myComplexType;

    public ImmutableClass(Integer integer, String string, MyComplexType myComplexType) {
        this.integer = integer;
        this.string = string;
        this.myComplexType = new MyComplexType(myComplexType.getInteger(), myComplexType.getString());
    }

    public Integer getInteger() {
        return integer;
    }

    public String getString() {
        return string;
    }

    public MyComplexType getMyComplexType() {
        return myComplexType;
    }

    @Override
    public String toString() {
        return "ImmutableClass{" +
                "integer=" + integer +
                ", string='" + string + '\'' +
                ", myComplexType=" + myComplexType +
                '}';
    }
}
