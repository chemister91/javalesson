package practice_patterns.immutable;

public class MyComplexType {
    private Integer integer;
    private String string;

    public MyComplexType(Integer integer, String string) {
        this.integer = integer;
        this.string = string;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "MyComplexType{" +
                "integer=" + integer +
                ", string='" + string + '\'' +
                '}';
    }
}
