package comparator;

import java.util.Arrays;

public class PersonArray {
    private static Person[] array;

    public PersonArray(Person[] array) {
        this.array = array;
    }

    public static void main(String[] args) {
        Person person1 = new Person("Ann", 28);
        Person person2 = new Person("Blad", 42);
        Person person21 = new Person("Wlad", 49);
        Person person3 = new Person("Cike", 35);
        Person person4 = new Person("Zom", 29);

        array = new Person[]{person1, person2, person21, person3, person4};
        System.out.println(Arrays.toString(array));
        //Arrays.sort(array);
        Arrays.sort(array, new PersonComparator());
        System.out.println(Arrays.toString(array));
    }

}
