package practice_patterns_2;

import practice_patterns_2.adapter.*;
import practice_patterns_2.lightweight.TourCatalog;
import practice_patterns_2.lightweight.TourDirections;

import java.awt.*;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {


}

    private static void testAdapter() {
        WindowHandler handler = new WindowHandler();
        SimpleWindow simpleWindow = new SimpleWindow(15, 46, Color.CYAN, "CYAN window", Collections.emptyList());
        DialogWindow dialogWindow = new DialogWindow(WindowOperations.CLOSE);
        handler.getParams(simpleWindow);
        handler.getParams(new DialogWindowAdapter(dialogWindow));
    }

    private static void testLightWeight() {
        TourCatalog tourCatalog = new TourCatalog();
        tourCatalog.createTour(TourDirections.ASIA, 200.65, "Аэрофлот", 12345, "PEK", "Золотой ветер", "Ли Юань");
        tourCatalog.createTour(TourDirections.ASIA, 870.23, "Аэрофлот", 12345, "PEK", "Золотой ветер", "Ли Юань");
        tourCatalog.createTour(TourDirections.RUSSIA, 78.14, "Аэрофлот", 54321, "DME", "Столица", "Иван Петров");
    }
}
