package practice_patterns_2.adapter;

import java.awt.*;
import java.util.Collection;

public interface Window {
    int getHeight();

    int getWidth();

    Color getColor();

    String getCaption();

    Collection<Button> getButtons();
}
