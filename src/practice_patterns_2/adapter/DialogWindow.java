package practice_patterns_2.adapter;

public class DialogWindow {
    public int HEIGHT = 214;
    public int WIDTH = 117;
    private WindowOperations operation;

    public DialogWindow(WindowOperations operation) {
        this.operation = operation;
    }

    public String getOperationName() {
        return operation.getTitle();
    }
}
