package practice_patterns_2.adapter;

import java.awt.*;
import java.util.Collection;

public class SimpleWindow implements Window {
    private int height;
    private int width;
    private Color color;
    private String caption;
    private Collection<Button> buttons;

    public SimpleWindow(int height, int width, Color color, String caption, Collection<Button> buttons) {
        this.height = height;
        this.width = width;
        this.color = color;
        this.caption = caption;
        this.buttons = buttons;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public String getCaption() {
        return caption;
    }

    @Override
    public Collection<Button> getButtons() {
        return buttons;
    }
}
