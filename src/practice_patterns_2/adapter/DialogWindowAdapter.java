package practice_patterns_2.adapter;

import java.awt.*;
import java.util.Collection;
import java.util.Collections;

public class DialogWindowAdapter implements Window {
    private DialogWindow window;

    public DialogWindowAdapter(DialogWindow window) {
        this.window = window;
    }

    @Override
    public int getHeight() {
        return window.HEIGHT;
    }

    @Override
    public int getWidth() {
        return window.WIDTH;
    }

    @Override
    public Color getColor() {
        return Color.WHITE;
    }

    @Override
    public String getCaption() {
        return window.getOperationName();
    }

    @Override
    public Collection<Button> getButtons() {
        return Collections.EMPTY_SET;
    }
}
