package practice_patterns_2.adapter;

public enum WindowOperations {
    OPEN("Открыть"),
    CLOSE("Закрыть");

    private String title;

    WindowOperations(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
