package practice_patterns_2.adapter;

public class WindowHandler {
    public void getParams(Window window) {
        System.out.println(window.getHeight());
        System.out.println(window.getWidth());
        System.out.println(window.getCaption());
        System.out.println(window.getButtons());
        System.out.println(window.getColor());
    }
}
