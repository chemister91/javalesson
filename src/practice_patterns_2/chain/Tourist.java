package practice_patterns_2.chain;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Tourist {
    private String name;
    private Integer passportNumber;
    private LocalDate expiredDate;
    private Integer boardingPassNumber;
    private Integer visaNumber;
    private String entryMark;

    public Tourist(String name, Integer passportNumber, LocalDate expiredDate, Integer boardingPassNumber, Integer visaNumber, String entryMark) {
        this.name = name;
        this.passportNumber = passportNumber;
        this.expiredDate = expiredDate;
        this.boardingPassNumber = boardingPassNumber;
        this.visaNumber = visaNumber;
        this.entryMark = entryMark;
    }

    public String getName() {
        return name;
    }

    public Boolean hasPassport() {
        return !passportNumber.equals(0);
    }

    public Boolean passportIsValid() {
        return ChronoUnit.DAYS.between(expiredDate, LocalDate.now()) > 90;
    }

    public Boolean hasBoardingPass() {
        return !boardingPassNumber.equals(0);
    }

    public Boolean hasVisa() {
        return !visaNumber.equals(0);
    }

    public Boolean hasEntryMark() {
        return !entryMark.isEmpty();
    }
}
