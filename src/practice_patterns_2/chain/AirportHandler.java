package practice_patterns_2.chain;

public abstract class AirportHandler {
    private AirportHandler next;

    public AirportHandler forwardTo(AirportHandler next) {
        this.next = next;
        return next;
    }

    public abstract Boolean check(Tourist tourist);

    protected Boolean checkNext(Tourist tourist) {
        if (next == null) {
            return true;
        } else {
            return next.check(tourist);
        }
    }
}
