package practice_patterns_2.lightweight;

public enum TourDirections {
    ASIA,
    EUROPE,
    RUSSIA;
}
