package practice_patterns_2.lightweight;

public abstract class Tour {
    private double cost;
    private TourParams params;

    public Tour(double cost, TourParams params) {
        this.cost = cost;
        this.params = params;
    }

    public double getCost() {
        return cost;
    }

    public abstract String getAbout();
}
