package practice_patterns_2.lightweight;

import java.util.Objects;

public class TourParams {
    private String airLine;
    private int flightNumber;
    private String arrivalAirport;
    private String travelAgency;
    private String guideName;

    public TourParams(String airLine, int flightNumber, String arrivalAirport, String travelAgency, String guideName) {
        this.airLine = airLine;
        this.flightNumber = flightNumber;
        this.arrivalAirport = arrivalAirport;
        this.travelAgency = travelAgency;
        this.guideName = guideName;
    }

    public String getAirLine() {
        return airLine;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public String getTravelAgency() {
        return travelAgency;
    }

    public String getGuideName() {
        return guideName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TourParams that = (TourParams) o;
        return flightNumber == that.flightNumber &&
                Objects.equals(airLine, that.airLine) &&
                Objects.equals(arrivalAirport, that.arrivalAirport) &&
                Objects.equals(travelAgency, that.travelAgency) &&
                Objects.equals(guideName, that.guideName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(airLine, flightNumber, arrivalAirport, travelAgency, guideName);
    }
}
