package practice_patterns_2.lightweight;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class LightWeighter {
    private static List<TourParams> tourParamsList = new LinkedList<>();
    private static Logger logger = Logger.getLogger("LightWeiter");

    private LightWeighter() {
    }

    public static TourParams getTourParams(String airLine, int flightNumber, String arrivalAirport, String travelAgency, String guideName) {
        TourParams tourParams = new TourParams(airLine, flightNumber, arrivalAirport, travelAgency, guideName);
        Integer index = tourParamsList.indexOf(tourParams);
        if (index.equals(-1)){
            tourParamsList.add(tourParams);
            logger.info("добавлен новый параметр тура");
            return tourParams;
        } else {
            logger.info("взят уже существующих параметр тура");
            return tourParamsList.get(index);
        }
    }
}
