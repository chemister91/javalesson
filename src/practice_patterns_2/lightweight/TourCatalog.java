package practice_patterns_2.lightweight;

import practice_patterns.builder.car_features.Light;
import practice_patterns_2.lightweight.tours.AsiaTour;
import practice_patterns_2.lightweight.tours.EuropeTour;
import practice_patterns_2.lightweight.tours.RussiaTour;

import java.util.ArrayList;
import java.util.List;

public class TourCatalog {
    private List<Tour> tours = new ArrayList<>();

    public void createTour(TourDirections tourDirection, Double cost, String airLine, int flightNumber, String arrivalAirport, String travelAgency, String guideName) {
        TourParams params = LightWeighter.getTourParams(airLine, flightNumber, arrivalAirport, travelAgency, guideName);
        Tour tour;
        switch (tourDirection) {
            case ASIA:
                tour = new AsiaTour(cost, params);
                break;
            case EUROPE:
                tour = new EuropeTour(cost, params);
                break;
            case RUSSIA:
            default:
                tour = new RussiaTour(cost, params);
        }
        tours.add(tour);
    }
}
