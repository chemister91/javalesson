package practice_patterns_2.lightweight.tours;

import practice_patterns_2.lightweight.Tour;
import practice_patterns_2.lightweight.TourParams;

public class EuropeTour extends Tour {
    private static String about = "Тур в Европу";

    public EuropeTour(double cost, TourParams params) {
        super(cost, params);
    }

    @Override
    public String getAbout() {
        return about;
    }
}
