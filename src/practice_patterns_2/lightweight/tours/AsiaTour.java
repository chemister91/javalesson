package practice_patterns_2.lightweight.tours;

import practice_patterns_2.lightweight.Tour;
import practice_patterns_2.lightweight.TourParams;

public class AsiaTour extends Tour {
    private static String about = "Тур в Азию";

    public AsiaTour(double cost, TourParams params) {
        super(cost, params);
    }

    @Override
    public String getAbout() {
        return about;
    }

}
