package ru.bakay.shop_basket;

public enum Messages {
    ADDPRODUCT("Добавлен товар: %s, %d шт."),
    REMOVEPRODUCT("Товар %s удален из корзины"),
    UPDATEPRODUCTCOUNT("%s, новое количество: %d шт."),
    PRODUCTQUANT("%s, кол-во: %d шт."),
    CLEARBASKET("Корзина очищена"),
    BASKETISEMPTY("Нет товаров в корзине"),
    PRODUCTSINBASKET("Товары в корзине:"),
    PRODUCTNOTEXIST("Товар %s отсутствует в корзине");
    private String text;

    Messages(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
