package ru.bakay.shop_basket;

public class ConsoleLogger implements Logger {
    public static final ConsoleLogger Instance = new ConsoleLogger();

    public static ConsoleLogger getInstance() {
        return Instance;
    }

    @Override
    public void add(Messages message, Object... args) {
        System.out.println(String.format(message.getText(), args));
    }
}
