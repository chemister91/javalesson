package ru.bakay.shop_basket;

public class Main {
    public static void main(String[] args) {
        ShopBasket shopBasket = new ShopBasket();
        test(shopBasket);
    }

    private static void test(ShopBasket basket){
        basket.addProduct("Кофе", 10);
        basket.addProduct("Кофе", 15);
        basket.printProductQuantity("Кофе");
        basket.updateProductQuantity("Кофе", 5);
        basket.clear();
        basket.printBasket();
        basket.addProduct("Чай", 1);
        basket.addProduct("Хлеб", 1);
        basket.addProduct("Молоко", 3);
        basket.removeProduct("Хлеб");
        basket.removeProduct("Конфеты");
        basket.addProduct("Бананы", 3);
        basket.printBasket();
    }
}
