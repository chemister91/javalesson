package ru.bakay.shop_basket;

import java.util.*;

public class ShopBasket implements Basket {
    public ShopBasket() {
        this.productMap = new HashMap<>();
        this.log = ConsoleLogger.getInstance();
    }

    private Map<String, Integer> productMap;
    private Logger log;

    @Override
    public void addProduct(String product, int quantity) {
        if (hasProduct(product)) {
            updateProductQuantity(product, getProductQuantity(product) + quantity);
        } else {
            productMap.put(product, quantity);
            log.add(Messages.ADDPRODUCT, product, quantity);
        }
    }

    @Override
    public void removeProduct(String product) {
        if (hasProduct(product)) {
            productMap.remove(product);
            log.add(Messages.REMOVEPRODUCT, product);
        } else {
            log.add(Messages.PRODUCTNOTEXIST, product);
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (hasProduct(product)) {
            productMap.put(product, quantity);
            log.add(Messages.UPDATEPRODUCTCOUNT, product, quantity);
        } else {
            log.add(Messages.PRODUCTNOTEXIST, product);
        }
    }

    @Override
    public void clear() {
        if (productMap.size() > 0) {
            productMap.clear();
            log.add(Messages.CLEARBASKET);
        } else {
            log.add(Messages.BASKETISEMPTY);
        }
    }

    @Override
    public List<String> getProducts() {
        return new ArrayList<>(productMap.keySet());
    }

    @Override
    public int getProductQuantity(String product) {
        int productQuantity = 0;
        if (hasProduct(product)) {
            productQuantity = productMap.get(product);
        } else {
            log.add(Messages.PRODUCTNOTEXIST, product);
        }
        return productQuantity;
    }

    public void printProductQuantity(String product) {
        log.add(Messages.PRODUCTQUANT, product, getProductQuantity(product));
    }

    public void printBasket() {
        if (productMap.size() > 0) {
            log.add(Messages.PRODUCTSINBASKET);
            for (String product : getProducts()) {
                printProductQuantity(product);
            }
        } else {
            log.add(Messages.BASKETISEMPTY);
        }
    }

    private boolean hasProduct(String product) {
        return productMap.containsKey(product);
    }
}
