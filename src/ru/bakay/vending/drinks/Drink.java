package ru.bakay.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}