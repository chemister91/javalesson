package ru.bakay.lesson_mathbox;

import java.util.logging.Logger;

import static ru.bakay.Common.*;

public class Main {
    public static void main(String[] args) {
        test();
    }

    private static void test(){
        final Logger logger = Logger.getLogger("tst");
        IMathBox mathBox = MathBox.getInstance();
        try {
            mathBox.add(10);
            mathBox.add(15);
            mathBox.delete(10);
            mathBox.add(25);
            mathBox.add(56);
            mathBox.add(70);
            logger.info(getWhiteText("Максимум: " + mathBox.getMax().toString()));
            logger.info(getWhiteText("Минимум: " + mathBox.getMin().toString()));
            logger.info(getWhiteText("Сумма: " + mathBox.getTotal().toString()));
            logger.info(getWhiteText("Среднее: " + mathBox.getAverage().toString()));
            logger.info(getWhiteText("Умножение на 2: " + mathBox.getMultiplied(2).toString()));
            logger.info(getWhiteText("Все элементы: " + mathBox.getMap().toString()));

        } catch (IllegalAccessException e) {
            logger.severe(e.getMessage());
        }
    }
}
