package ru.bakay.lesson_mathbox;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IMathBox {

    @LogData
    void add(int value) throws IllegalAccessException;

    @LogData
    void delete(int value) throws IllegalAccessException;

    Integer getTotal();

    Double getAverage();

    Integer getMax();

    Integer getMin();

    Collection getMultiplied(int multiplicator);

    @LogData
    @ClearData
    Map<Integer, String> getMap();

    Collection getCollection();

    void clear();
}
