package ru.bakay.lesson_multithreading;

public enum Algorithm {
    SIMPLE,
    MULTITHREAD,
    SMART_SIMPLE,
    SMART_MULTITHREAD;
}
