package ru.bakay.lesson_multithreading;

import java.util.Collection;
import java.util.List;

public interface IFactorialCalculator {

    void calculate(int number, Algorithm algorithm);

    void calculate(Collection<Integer> numberList, Algorithm algorithm);
}
