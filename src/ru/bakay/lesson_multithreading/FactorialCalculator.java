package ru.bakay.lesson_multithreading;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static ru.bakay.Common.*;

public class FactorialCalculator implements IFactorialCalculator {
    private static final String MSG_INVALID_VALUE = "Невозможно вычислить факториал отрицательного числа (%d)";
    private static final String MSG_DURATION = "%d! длительность: %d ms";
    private static final String MSG_CALC_RESULT = "%d!: %s";
    private static final String MSG_TOO_LONG_RESULT = "%d!: too long value";
    private static final String MSG_START_POOL = "Вычисление пула чисел, размер: %d";
    private static final String MSG_POOL_DURATION = "Вычисление пула чисел, длительность: %d ms";

    private static final Logger logger = Logger.getLogger("FactorialCalculator");
    private Algorithm algorithm;

    private static<T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> com) {
        return CompletableFuture.allOf(com.toArray(new CompletableFuture<?>[com.size()]))
                .thenApply(v -> com.stream()
                        .map(CompletableFuture::join)
                        .collect(toList())
                );
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
        logger.info(getBlueText("Алгоритм: " + algorithm.toString()));
    }

    @Override
    public void calculate(int number, Algorithm algorithm) {
        setAlgorithm(algorithm);
        tryAndLog(number);
    }

    @Override
    public void calculate(Collection<Integer> numberList, Algorithm algorithm) {
        setAlgorithm(algorithm);
        tryAndLog(numberList);
    }

    private Collection<BigInteger> tryAndLog(Collection<Integer> numberList){
        logger.info(getBlueText(String.format(MSG_START_POOL, numberList.size())));
        long startTime = System.currentTimeMillis();
        Collection<BigInteger> result = calcByAlgorithm(numberList);
        logger.info(getBlueText(String.format(MSG_POOL_DURATION, System.currentTimeMillis() - startTime)));
        return result;
    }

    private BigInteger tryAndLog(int number){
        BigInteger result;
        if (number == 0) {
            result = BigInteger.ONE;
            logger.info(String.format(MSG_CALC_RESULT, number, result.toString()));
        } else if (number < 0) {
            result = BigInteger.ZERO;
            logger.warning(String.format(MSG_INVALID_VALUE, number));
        } else {
            long startTime = System.currentTimeMillis();
            result = calcByAlgorithm(number);
            logger.info(getBlueText(String.format(MSG_DURATION, number, System.currentTimeMillis() - startTime)));
            if (result.bitLength() > 1000) {
                logger.info(getWhiteText(String.format(MSG_TOO_LONG_RESULT, number)));
            } else {
                logger.info(getWhiteText(String.format(MSG_CALC_RESULT, number, result.toString())));
            }
        }
        return result;
    }

    private BigInteger calcByAlgorithm(int number) {
        switch (algorithm) {
            case SIMPLE:
            case MULTITHREAD:
                return calcFactorial(number, algorithm.equals(Algorithm.MULTITHREAD));
            case SMART_SIMPLE:
            case SMART_MULTITHREAD:
                return calcFactorial(prepareForCalculate(number), algorithm.equals(Algorithm.SMART_MULTITHREAD));
            default:
                return calcFactorial(number, false);
        }
    }

    private Collection<BigInteger> calcByAlgorithm(Collection<Integer> numberList){
        switch (algorithm){
            case MULTITHREAD:
            case SMART_MULTITHREAD:
                return calcFactorialListWithThreadPoll(numberList);
            case SIMPLE:
            case SMART_SIMPLE:
            default:
                return calcFactorialList(numberList);
        }
    }

    private Collection<BigInteger> calcFactorialList(Collection<Integer> numberList) {
        return numberList
                .stream()
                .map(this::tryAndLog)
                .collect(toList());
    }

    private Collection<BigInteger> calcFactorialListWithThreadPoll(Collection<Integer> numberList) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        List<CompletableFuture<BigInteger>> futures = new ArrayList<>();
        numberList.forEach(number ->
                futures.add(
                        CompletableFuture.supplyAsync(
                                () -> tryAndLog(number), executorService
                        )
                ));
        try {
            return sequence(futures).get();
        } catch (InterruptedException | ExecutionException e) {
            logger.severe(e.toString());
            return null;
        } finally {
            executorService.shutdown();
        }
    }

    private BigInteger calcFactorial(Collection<BigInteger> bigIntegerSet, boolean useParallelStream) {
        Stream<BigInteger> bigIntegerStream =
                bigIntegerSet
                .stream();
        if (useParallelStream) bigIntegerStream = bigIntegerStream.parallel();
        return bigIntegerStream
                .reduce(BigInteger::multiply)
                .get();
    }

    private BigInteger calcFactorial(int number, boolean useParallelStream) {
        IntStream intStream =
                IntStream
                .rangeClosed(1, number);
        if (useParallelStream) intStream = intStream.parallel();
        return intStream
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger::multiply)
                .get();
    }

    /**
     * Метод генерирует коллекцию чисел, необходимых для вычисления факториала заданного числа
     * Коллекция получается в 2 раза меньше, чем при использовании стандартного заполнения (1,2,3,4,5..)
     * @param number число, факториал которого нужно получить
     * @return коллекция чисел для выполнения умножения
     */
    private Collection<BigInteger> prepareForCalculate(int number) {
        List<BigInteger> bigIntegerList = new ArrayList<>();
        BigInteger elementValue = BigInteger.ZERO;
        int addValue = number;

        while (addValue >= 1) {
            if (addValue > 1) {
                elementValue = elementValue.add(BigInteger.valueOf(addValue));
            } else {
                elementValue = BigInteger.valueOf((int) Math.ceil(number / 2f));
            }
            bigIntegerList.add(elementValue);
            addValue -= 2;
        }
        return bigIntegerList;
    }
}
