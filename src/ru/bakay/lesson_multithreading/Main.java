package ru.bakay.lesson_multithreading;

import static ru.bakay.Common.*;

public class Main {
    public static void main(String[] args) {
        test();
    }

    private static void test(){
        FactorialCalculator fc = new FactorialCalculator();

        // тест на отрицательные числа, 0, 1
        fc.calculate(-1, Algorithm.SIMPLE);
        fc.calculate(0, Algorithm.SMART_SIMPLE);
        fc.calculate(1, Algorithm.SIMPLE);

        // по одному числу
        fc.calculate(100_000, Algorithm.SIMPLE);
        fc.calculate(105_000, Algorithm.SMART_SIMPLE);
        fc.calculate(123_456, Algorithm.MULTITHREAD);
        fc.calculate(123_654, Algorithm.SMART_MULTITHREAD);

        // пул чисел
        fc.calculate(getRandomList(50_000, 90_000, 100), Algorithm.SIMPLE);
        fc.calculate(getRandomList(45_000, 95_000, 100), Algorithm.SMART_SIMPLE);
        fc.calculate(getRandomList(100_000, 120_000, 100), Algorithm.MULTITHREAD);
        fc.calculate(getRandomList(100_000, 120_000, 100), Algorithm.SMART_MULTITHREAD);
    }
}
