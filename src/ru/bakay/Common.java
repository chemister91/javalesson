package ru.bakay;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Common {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_WHITE = "\u001B[38m";

    public static String getColorText(String text, String color) {
        return color + text + ANSI_RESET;
    }

    public static String getWhiteText(String text) {
        return getColorText(text, ANSI_WHITE);
    }

    public static String getBlueText(String text) {
        return getColorText(text, ANSI_BLUE);
    }

    public static List<Integer> getRandomList(int minValue, int maxValue, int size) {
        return IntStream
                .generate(() ->
                {
                    return ThreadLocalRandom.current().nextInt(minValue, maxValue);
                })
                .limit(size)
                .boxed()
                .collect(toList());
    }
}
