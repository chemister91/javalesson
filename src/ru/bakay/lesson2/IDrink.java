package ru.bakay.lesson2;

public interface IDrink {
    double Price();

    String Title();

    DrinkType Type();
}
