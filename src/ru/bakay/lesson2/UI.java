package ru.bakay.lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

class ConsoleUI implements IUI {
    enum Operations {ADMIN, SALE, CASHOUT, DRINK, EXIT, EMPTY}

    private VendingMachine vm = new VendingMachine();
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private String inputString;
    private int choice;

    @Override
    public void Run() {
        Init();
        Operations operation = Operations.EMPTY;
        do {
            try {
                System.out.println("\nВыберите режим работы \n SALE - продажи \n ADMIN - обслуживание \n EXIT - завершение работы");
                inputString = reader.readLine();
                operation = Operations.valueOf(inputString);
                switch (operation) {
                    case ADMIN:
                        EnterAdminMode();
                        break;
                    case SALE:
                        EnterSaleMode();
                        break;
                    case EXIT:
                        System.out.println("Завершение работы...");
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            } catch (IllegalArgumentException ex) {
                System.out.println("Неверная команда!");
            } catch (Throwable e) {
                System.out.println("Непредвиденная ошибка: " + e.getMessage());
            }
        }
        while (!operation.equals(Operations.EXIT));
    }

    @Override
    public void EnterAdminMode() {
        Operations operation = Operations.EMPTY;
        while (!operation.equals(Operations.EXIT)) {
            try {
                System.out.println("\nОБСЛУЖИВАНИЕ\nВыберите действие: \n CASHOUT - Инкассация \n DRINK - Добавление напитков");
                inputString = reader.readLine();

                operation = Operations.valueOf(inputString);
                switch (operation) {
                    case CASHOUT:
                        System.out.println("\nИНКАССАЦИЯ");
                        if (vm.HasMoney()) {
                            PleaseWait("$");
                            System.out.println(String.format("Выручка: %.2f", vm.GetRevenue()));
                            vm.CashOut();
                        } else {
                            System.out.println("Нет денег для инкассации");
                        }
                        break;
                    case DRINK:
                        AddDrink();
                        break;
                    case EXIT:
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            } catch (IllegalArgumentException ex) {
                System.out.println("Неверное значение!");
                EnterAdminMode();
            } catch (IOException ex) {
                System.out.println("Ошибка ввода-вывода: " + ex.getMessage());
            }
        }
    }

    @Override
    public void EnterSaleMode() {
        while (Greet()) {
            try {

                AskDrink();

                AskMoney();

                ReturnChange();

                CloseReceipt();

            } catch (IllegalArgumentException ex) {
                System.out.println("Операция невозможна: " + ex.getMessage());
            }
        }
    }

    private boolean Greet() {
        try {
            vm.PrintDrinks();
            System.out.print("Выберите напиток: ");
            inputString = reader.readLine();
            return !inputString.equals(Operations.EXIT.toString());

        } catch (IOException ex) {
            System.out.println("Операция невозможна: " + ex.getMessage());
            return true;
        }
    }

    @Override
    public void AskDrink() {
        try {
            choice = Integer.parseInt(reader.readLine());
            if (!vm.HasDrink(choice)) {
                throw new IllegalArgumentException("неверный номер напитка");
            }
            System.out.println(String.format("Ваш выбор: %s", vm.GetDrinkTitle(choice)));
        } catch (IOException ex) {
            System.out.println("Операция невозможна: " + ex.getMessage());
        }
    }

    @Override
    public void AskMoney() {
        do {
            System.out.println(String.format("\nОсталось внести %.2f рублей", vm.GetRestSum(choice)));
            System.out.print("Внесите деньги: ");
            try {
                vm.AddMoney(Math.abs(Double.parseDouble(reader.readLine())));
            } catch (NumberFormatException | IOException ex) {
                System.out.println("Введите число!");
            }
        } while (vm.GetRestSum(choice) > 0);
    }

    @Override
    public void ReturnChange() {
        double restSum = vm.GetRestSum(choice);
        if (restSum < 0) {
            System.out.println(String.format("\nЗаберите сдачу: %.2f", Math.abs(restSum)));
            vm.ReturnChange(restSum);
        }
    }

    @Override
    public void CloseReceipt() {
        System.out.println("\nПриготовление напитка");
        PleaseWait("=");
        System.out.println("\nНапиток готов!");
        System.out.println("\nБлагодарим за использование вендингового автомата");
        PleaseWait("|");
        vm.CloseReceipt();
    }

    private void PleaseWait(String chr) {
        for (int i = 0; i <= 35; i++) {
            System.out.print(String.format("|%-35s|\r", String.join("", Collections.nCopies(i, chr))));
            try {
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void AddDrink() {
        Operations operation = Operations.EMPTY;
        while (!operation.equals(Operations.EXIT)) {
            System.out.println("Напитки в наличии");
            vm.PrintDrinks();
            try {
                System.out.println("\nДОБАВЛЕНИЕ НАПИТКОВ");

                System.out.print("КОД: ");
                int plu = Integer.parseInt(reader.readLine());

                System.out.print("НАИМЕНОВАНИЕ: ");
                String title = reader.readLine();

                System.out.print("ЦЕНА: ");
                double price = Double.parseDouble(reader.readLine());

                System.out.print("ТИП НАПИТКА (COLD/HOT): ");
                DrinkType type = DrinkType.valueOf(reader.readLine());

                vm.AddDrink(plu, title, price, type);
                System.out.println("Напиток успешно добавлен");

            } catch (IllegalArgumentException | IOException ex) {
                System.out.println("Неверные параметры: " + ex.getMessage());
            } finally {
                System.out.println("\nДобавить еще один напиток?\n Y - да \n EXIT - выход");
                try {
                    inputString = reader.readLine();
                    if (inputString.equals(Operations.EXIT.toString())) {
                        operation = Operations.EXIT;
                    }
                } catch (IOException ex) {
                    System.out.println("Ошибка ввода-вывода: " + ex.getMessage());
                }
            }
        }
    }

    private void Init() {
        System.out.println("Инициализация...");
        vm.AddDrink(1, "Чай с сахаром", 30, DrinkType.HOT);
        vm.AddDrink(2, "Капучино", 90, DrinkType.HOT);
        vm.AddDrink(3, "Латте", 110, DrinkType.HOT);
        vm.AddDrink(4, "Кока кола", 80, DrinkType.COLD);
        vm.AddDrink(5, "Питьевая вода", 10, DrinkType.COLD);
    }
}
