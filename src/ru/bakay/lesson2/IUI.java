package ru.bakay.lesson2;

public interface IUI {
    void Run();

    void EnterAdminMode();

    void EnterSaleMode();

    void AddDrink();

    void AskDrink();

    void AskMoney();

    void ReturnChange();

    void CloseReceipt();
}
