package ru.bakay.lesson2;

public enum DrinkType {
    HOT("Горячие напитки"),
    COLD("Холодные напитки");
    private String title;

    DrinkType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
