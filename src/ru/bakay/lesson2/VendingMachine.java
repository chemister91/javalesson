package ru.bakay.lesson2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class Drink implements IDrink {
    private double price;
    private String title;
    private DrinkType type;

    Drink(String title, double price, DrinkType type) {
        this.title = title;
        this.price = price;
        this.type = type;
    }

    @Override
    public double Price() {
        return price;
    }

    @Override
    public String Title() {
        return title;
    }

    @Override
    public DrinkType Type() {
        return type;
    }
}

public class VendingMachine {
    private double moneyAmount = 0;
    private double userMoney = 0;
    private Map<Integer, Drink> drinks = new HashMap<>();

    public double GetRevenue() {
        return moneyAmount;
    }

    public void CashOut() {
        moneyAmount = 0;
    }

    public void AddDrink(int PLU, String title, double price, DrinkType type) {
        drinks.put(PLU, new Drink(title, price, type));
    }

    public boolean HasMoney() {
        return (moneyAmount != 0);
    }

    public boolean HasDrink(int PLU) {
        return drinks.containsKey(PLU);
    }

    public void AddMoney(double userMoney) {
        this.userMoney += userMoney;
    }

    public double GetRestSum(int PLU) {
        return drinks.get(PLU).Price() - userMoney;
    }

    public String GetDrinkTitle(int PLU) {
        return drinks.get(PLU).Title();
    }

    public void ReturnChange(double Sum) {
        userMoney += Sum;
    }

    public void CloseReceipt() {
        moneyAmount += userMoney;
        userMoney = 0;
    }

    public void PrintDrinks() {
        String strTemplate = "| %2s | %20s | %5s |";
        String delimiter = String.join("", Collections.nCopies(37, "-"));
        for (DrinkType drinkType : DrinkType.values()) {
            System.out.println(delimiter);
            System.out.println(String.format("|%7s %-19s %7s|", " ", drinkType.getTitle(), " "));
            System.out.println(delimiter);
            drinks.entrySet().forEach(entry -> {
                Drink drink = entry.getValue();
                if (drink.Type().equals(drinkType)) {
                    System.out.println(String.format(strTemplate, entry.getKey(), drink.Title(), drink.Price()));
                }
            });
        }
        System.out.println(delimiter);
    }


}
