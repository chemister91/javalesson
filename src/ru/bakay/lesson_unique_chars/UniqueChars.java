package ru.bakay.lesson_unique_chars;

import java.util.Map;
import java.util.TreeMap;

public class UniqueChars {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        text = convertMapToString(analyzeFrequency());
    }

    private Map<Character, Integer> analyzeFrequency(){
        Map<Character, Integer> charMap = new TreeMap<>();
        for(char c : text.toCharArray()) {
            int count = 1;
            if (charMap.containsKey(c)){
                count += charMap.get(c);
            }
            charMap.put(c, count);
        }
        return charMap;
    }

    private String convertMapToString(Map<Character, Integer> charMap) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<Character, Integer> entry : charMap.entrySet()) {
            stringBuilder.append(String.format("\"%s\": \t %d \n", entry.getKey().toString(), entry.getValue()));
        }
        return stringBuilder.toString();
    }
}
