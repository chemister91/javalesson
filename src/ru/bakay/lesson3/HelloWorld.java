package ru.bakay.lesson3;

public class HelloWorld {
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_GREEN = "\u001B[32m";


    private enum Messages {
        HELLO("Hello, World!"),
        NPE("This is Null Pointer Exception"),
        OUTOFBOUNDS("This is Array Index Out Of Bounds Exception"),
        CUSTOMEXCEPTION("This is Custom Exception"),
        SUCCESS("The test completed successfully");
        private String text;

        Messages(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public String getColorText(String Color) {
            return Color + text + ANSI_RESET;
        }
    }

    public void Run() {
        System.out.println(Messages.HELLO.getText());
        ThrowNPE();
        ThrowOutOfBoundException();
        ThrowCustomException();
        System.out.println(Messages.SUCCESS.getColorText(ANSI_GREEN));
    }

    private void ThrowNPE() {
        try {
            Integer a = null;
            a.hashCode();
        } catch (NullPointerException e) {
            System.out.println(Messages.NPE.getColorText(ANSI_RED));
        }
    }

    private void ThrowOutOfBoundException() {
        try {
            int[] array = new int[10];
            array[20] = 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(Messages.OUTOFBOUNDS.getColorText(ANSI_RED));
        }
    }

    private void ThrowCustomException() {
        try {
            throw new IllegalAccessException();
        } catch (Throwable e) {
            System.out.println(String.format("%s: %s", Messages.CUSTOMEXCEPTION.getColorText(ANSI_RED), e.toString()));
        }
    }
}
