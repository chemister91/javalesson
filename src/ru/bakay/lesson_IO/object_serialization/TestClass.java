package ru.bakay.lesson_IO.object_serialization;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

public class TestClass {
    public String stringValue = "test";
    public int intValue = 10;
    private boolean aBoolean = false;
    public Double aDouble = 1.1;
    private char aChar = 'y';

    public List<String> getFields() {
        List<String> fieldList = new LinkedList<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                fieldList.add(String.format("%s %s %s", field.getGenericType().getTypeName(), field.getName(), field.get(this)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return fieldList;
    }
}
