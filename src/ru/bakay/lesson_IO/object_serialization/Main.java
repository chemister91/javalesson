package ru.bakay.lesson_IO.object_serialization;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Parser parser = new Parser();
        Logger logger = Logger.getLogger("Test");
        String fileName = "C:\\test\\object.txt";

        parser.writeObjectToFile(new TestClass(), fileName);
        TestClass testClass = (TestClass) parser.readObjectFromFile(fileName);
        logger.info(testClass.getFields().toString());
    }
}
