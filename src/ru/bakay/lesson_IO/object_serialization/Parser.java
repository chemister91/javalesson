package ru.bakay.lesson_IO.object_serialization;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static ru.bakay.lesson_IO.file_reader.FileIO.readFromFile;
import static ru.bakay.lesson_IO.file_reader.FileIO.writeToFile;

public class Parser {
    final int FIELD_TYPE = 0;
    final int FIELD_NAME = 1;
    final int FIELD_VALUE = 2;

    private final Logger logger = Logger.getLogger("Parser");

    public void writeObjectToFile(Object object, String fileName) {
        try {
            writeToFile(serialize(object), fileName);
        } catch (IllegalAccessException | IOException e) {
            logger.severe(e.toString());
        }
    }

    public Object readObjectFromFile(String fileName) {
        try {
            return deSerialize(readFromFile(fileName));
        } catch (Throwable e) {
            logger.severe(e.toString());
            return new Object();
        }
    }

    private List<String> serialize(Object object) throws IllegalAccessException {
        List<String> objectList = new ArrayList<>();
        objectList.add(object.getClass().getName());
        for (Field field : object.getClass().getDeclaredFields()) {
            try {
                setFieldAccessible(field, true);
                StringBuilder builder = new StringBuilder()
                        .append(field.getGenericType().getTypeName())
                        .append(":")
                        .append(field.getName())
                        .append(":")
                        .append(field.get(object));
                objectList.add(builder.toString());
            } finally {
                setFieldAccessible(field, false);
            }
        }
        return objectList;
    }

    private Object deSerialize(List<String> objectList) throws Throwable {
        Object object = createObject(objectList.get(0));
        for (int i = 1; i < objectList.size(); i++) {
            List<String> fieldPropList = Arrays.asList(objectList.get(i).split("\\s*:\\s*"));
            Field field = object
                    .getClass()
                    .getDeclaredField(fieldPropList.get(FIELD_NAME));
            try {
                setFieldAccessible(field, true);
                field.set(object, castFieldValue(fieldPropList.get(FIELD_TYPE), fieldPropList.get(FIELD_VALUE)));
            } finally {
                setFieldAccessible(field, false);
            }
        }
        return object;
    }

    private void setFieldAccessible(Field field, boolean accessible) {
        if (Modifier.isPrivate(field.getModifiers())) {
            field.setAccessible(accessible);
        }
    }

    private Object castFieldValue(String fieldType, String fieldValue) throws IllegalArgumentException {
        switch (fieldType) {
            case "java.lang.String":
                return fieldValue;
            case "java.lang.BigInteger":
                return new BigInteger(fieldValue);
            case "java.lang.BigDecimal":
                return new BigDecimal(fieldValue);
            case "java.lang.Integer":
            case "int":
                return Integer.valueOf(fieldValue);
            case "java.lang.Double":
            case "double":
                return Double.valueOf(fieldValue);
            case "java.lang.Boolean":
            case "boolean":
                return Boolean.valueOf(fieldValue);
            case "java.lang.Byte":
            case "byte":
                return Byte.valueOf(fieldValue);
            case "java.lang.Short":
            case "short":
                return Short.valueOf(fieldValue);
            case "java.lang.Long":
            case "long":
                return Long.valueOf(fieldValue);
            case "java.lang.Float":
            case "float":
                return Float.valueOf(fieldValue);
            case "java.lang.Character":
            case "char":
                return fieldValue.charAt(0);
            default:
                throw new IllegalArgumentException("Данный тип нельзя преобразовать");
        }
    }

    private Object createObject(String className) throws Throwable {
        Class<?> aClass = Class.forName(className);
        Constructor<?> ctor = aClass.getConstructor();
        return ctor.newInstance();
    }
}
