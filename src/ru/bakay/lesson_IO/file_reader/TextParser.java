package ru.bakay.lesson_IO.file_reader;

import java.io.IOException;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static ru.bakay.lesson_IO.file_reader.FileIO.readFromFile;
import static ru.bakay.lesson_IO.file_reader.FileIO.writeToFile;

public class TextParser {
    private SortedSet<String> wordSet;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public SortedSet<String> getWordSet() {
        return wordSet;
    }

    public void searchUniqueWords(String readFrom, String saveTo) {
        try {
            wordSet = new TreeSet<>();
            readFromFile(readFrom).forEach(this::splitToSet);
            writeToFile(wordSet, saveTo);
        } catch (IOException e) {
            logger.severe(e.toString());
        }
    }

    private void splitToSet(String text) {
        Stream
                .of(text.split("[^A-Za-zА-Яа-я0-9]+"))
                .map(String::toLowerCase)
                .distinct()
                .forEach(wordSet::add);
    }
}
