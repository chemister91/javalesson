package ru.bakay.lesson_IO.file_reader;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Logger logger = Logger.getLogger("Test");
        TextParser textParser = new TextParser();
        textParser.searchUniqueWords("C:\\Logs\\_20181201.log", "C:\\test\\parsed.txt");
        logger.info(textParser.getWordSet().toString());
    }
}
