package ru.bakay.lesson_IO.file_reader;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

public class FileIO {
    public static void writeToFile(Collection<String> stringList, String fileName) throws IOException {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            for (String str : stringList) {
                fileWriter.write(str + "\r\n");
            }
        }
    }

    public static List<String> readFromFile(String fileName) throws IOException {
        try {
            return Files.readAllLines(Paths.get(fileName));
        } catch (MalformedInputException e) {
            return Files.readAllLines(Paths.get(fileName), Charset.forName("windows-1251"));
        }
    }
}
