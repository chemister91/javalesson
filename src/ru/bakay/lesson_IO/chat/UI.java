package ru.bakay.lesson_IO.chat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;

public class UI {
    private final String MSG_START_INIT = "Initialization...";
    private final String MSG_FINISH_INIT = "Initialization done";
    private final String JFRAME_TITLE = "MyChat";
    private final String JBUTTON_TEXT = "Send!";
    private final String CHAT_PATTERN = "%s: %s";

    private JFrame jFrame;
    private JTextField textField;
    private JTextField userNameField;
    private JTextArea textArea;
    private Chat chat;
    private String userName;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public UI() {
        EventQueue.invokeLater(() -> {
            try {
                initialize();
                jFrame.setVisible(true);
            } catch (Exception e) {
                logger.severe(e.toString());
            }
        });
    }

    private void initialize() {
        logger.info(MSG_START_INIT);
        this
                .createJFrame()
                .createJTextArea()
                .createJTextField()
                .createUserNameField()
                .createSendButton()
                .initChat();
        logger.info(MSG_FINISH_INIT);
    }

    private UI createJFrame() {
        jFrame = new JFrame();
        jFrame.setTitle(JFRAME_TITLE);
        jFrame.setBounds(100, 110, 326, 380);
        jFrame.getContentPane().setLayout(null);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setResizable(false);
        return this;
    }

    private UI createJTextArea() {
        textArea = new JTextArea();
        textArea.setBounds(36, 143, 234, 181);
        textArea.setEditable(false);
        jFrame.getContentPane().add(textArea);
        return this;
    }

    private UI createJTextField() {
        textField = new JTextField();
        textField.setBounds(36, 50, 234, 23);
        jFrame.getContentPane().add(textField);
        return this;
    }

    private UI createUserNameField() {
        userName = System.getProperty("user.name");
        userNameField = new JTextField();
        userNameField.setBounds(36, 20, 234, 23);
        userNameField.setText("You: " + userName);
        userNameField.setEditable(false);
        jFrame.getContentPane().add(userNameField);
        return this;
    }

    private UI createSendButton() {
        JButton sendButton = new JButton(JBUTTON_TEXT);
        sendButton.addActionListener(this::sendMessage);
        sendButton.setBounds(180, 90, 89, 23);
        jFrame.getContentPane().add(sendButton);
        return this;
    }

    private UI initChat() {
        try {
            chat = new Chat(this, textArea::append);
        } catch (Exception e) {
            logger.severe(e.toString());

        }
        return this;
    }

    private void sendMessage(ActionEvent actionEvent) {
        if (!textField.getText().isEmpty()) {
            String message = textField.getText() + "\r\n";
            try {
                chat.sendMessage(String.format(CHAT_PATTERN, userName, message));
            } catch (Exception e) {
                logger.warning(e.toString());
            }
            textField.setText("");
            textField.requestFocus();
        }
    }
}