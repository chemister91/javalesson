package ru.bakay.lesson_IO.chat;

import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class Chat {
    private static final int PORT = 1234;
    private static final String BROADCAST_IP = "10.0.0.255";
    private MulticastSocket datagramSocket;
    private MulticastSocket multicastSocket;
    private Consumer<String> onGetMessage;
    private Object syncObject;

    private static Logger logger = Logger.getLogger("Chat");

    private Runnable messageReceiver = () -> {
        byte buf[] = new byte[1024];
        while (!Thread.currentThread().isInterrupted()) {
            try {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                multicastSocket.receive(packet);
                String received = new String(
                        packet.getData(),
                        0,
                        packet.getLength(),
                        StandardCharsets.UTF_8
                );
                synchronized (syncObject) {
                    onGetMessage.accept(received);
                }
            } catch (Exception e) {
                logger.warning(e.toString());
            }
        }
    };

    public Chat(Object syncObject, Consumer<String> onGetMessage) throws Exception {
        applySettings();
        this.syncObject = syncObject;
        this.onGetMessage = onGetMessage;
        new Thread(messageReceiver).start();
    }

    public void sendMessage(String message) throws Exception {
        byte buf[] = message.getBytes(StandardCharsets.UTF_8);
        datagramSocket.send(
                new DatagramPacket(
                        buf,
                        buf.length,
                        getBroadcastAddress(),
                        PORT
                )
        );
    }

    private void applySettings() throws Exception {
        datagramSocket = new MulticastSocket();
        datagramSocket.setBroadcast(true);
        datagramSocket.setReuseAddress(true);
        multicastSocket = new MulticastSocket(PORT);
        if (getBroadcastAddress().isMulticastAddress()) {
            multicastSocket.joinGroup(getBroadcastAddress());
        }
    }

    private InetAddress getBroadcastAddress() throws Exception {
        return InetAddress.getByName(BROADCAST_IP);
    }
}