package ru.bakay.lesson1.task3;

import java.util.Scanner;

public class MultiTableGenerator {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        System.out.println("Введите верхнюю границу");
        do {
            if (console.hasNextInt()) {
                generate(console.nextInt());
                break;
            } else {
                System.out.println("Введите число!");
                console.next();
            }
        } while (true);
    }

    private static void generate(int maxValue) {
        String strTemplate = "%-10s";
        System.out.println("Таблица умножения");
        for (int ln = 1; ln <= maxValue; ln++) {
            StringBuilder builder = new StringBuilder();
            for (int ch = 1; ch <= maxValue; ch++) {
                if (ch == 1) {
                    if (ln != 1) {
                        builder.append(String.format(strTemplate, ln));
                    } else {
                        builder.append(String.format(strTemplate,"#"));
                    }
                } else {
                    if (ln == 1) {
                        builder.append(String.format(strTemplate, ch));
                    } else {
                        builder.append(String.format(strTemplate, ln * ch));
                    }
                }
            }
            System.out.println(builder.toString());
        }
    }
}
