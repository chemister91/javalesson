package ru.bakay.lesson1.task4;

import java.math.BigInteger;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        System.out.println("Введите число N");
        do {
            if (console.hasNextInt()) {
                generate(console.nextInt());
                break;
            } else {
                System.out.println("Введите число!!!");
                console.next();
            }
        } while (true);
    }

    private static void generate(int N) {
        BigInteger currentValue;
        String strFormat = "%3d!: %" + (int)(N * 1.5) + "s";
        currentValue = new BigInteger("1");
        System.out.println(String.format("Факториалы первых %d чисел", N));
        for (int x = 1; x <= N; x++) {
            currentValue = currentValue.multiply(BigInteger.valueOf(x));
            System.out.println(String.format(strFormat, x, currentValue.toString()));
        }
    }
}
