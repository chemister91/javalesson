package ru.bakay.lesson1.task1;

import java.util.Scanner;

public class FuelCalculator {

    public static void main(String[] args) {
        int fuelPrice;
        Scanner console = new Scanner(System.in);
        System.out.println("Введите стоимость топлива:");
        fuelPrice = console.nextInt();
        do {
            System.out.println("Введите объем топлива:");
            if (console.hasNextInt()){
                calculate(console.nextInt(), fuelPrice);
            } else if (console.hasNext("exit")) {
                break;
            } else {
                System.out.println("Введите число!");
                console.next();
            }

        }
        while (true);
        console.close();
    }


    private static void calculate(int fuelVolume, int fuelPrice) {
        System.out.println("К оплате:" + fuelVolume * fuelPrice);
    }
}
