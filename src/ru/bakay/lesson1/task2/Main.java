package ru.bakay.lesson1.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        ArrayList<Integer> intList = new ArrayList<>();
        System.out.println("Введите четыре числа");
        for (int x = 0; x < 4; x++) {
            do {
                if (console.hasNextInt()) {
                    intList.add(console.nextInt());
                    break;
                } else {
                    System.out.println("Введите число!");
                    console.next();
                }
            } while (true);
        }
        console.close();
        Collections.sort(intList);
        System.out.println("Наименьшее число: " + intList.get(0));
    }
}
