package ru.bakay.lessonOOP;

import java.util.LinkedList;
import java.util.List;

public abstract class Client {
    private String fullName;
    private List<Account> accountList;
    private ClientType clientType;

    public Client(String fullName, ClientType clientType) {
        this.fullName = fullName;
        this.accountList = new LinkedList<>();
        this.clientType = clientType;
    }

    public String getClientTypeName() {
        return clientType.getTitle();
    }

    public String getFullName() {
        return this.fullName;
    }

    public double getTotalBalance() {
        double balance = 0;
        for (Account account : this.accountList) {
            balance += account.getRealSum();
        }
        return balance;
    }

    public List<Account> getAccountList() {
        return this.accountList;
    }
}
