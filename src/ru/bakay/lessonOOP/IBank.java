package ru.bakay.lessonOOP;

public interface IBank {
    String issueCard(int clientID, AccountType cardType);

    void blockCard(int clientID, String cardNumber);

    int addClient(String fullName, String uniqueIdent, ClientType clientType);

    void deleteClient(int clientID);

    Client getClient(int clientID);

    void issueLoan(int clientID, double loanSum);

    void createDeposit(int clientID, double depositSum);

    double calculateBalance();
}
