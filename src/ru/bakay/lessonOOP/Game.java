package ru.bakay.lessonOOP;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;

public class Game {
    private enum Operations {
        ADDCLIENT,
        DELETECLIENT,
        ISSUECARD,
        BLOCKCARD,
        ISSUELOAN,
        CREATEDEPOSIT
    }

    private enum Messages {
        NEWCLIENT("Новый клиент: %s, %s"),
        DELETEDCLIENT("Клиент %s закрыл все счета на сумму %.2f рублей"),
        NEWCARD("Клиент %s выпустил карту №%s"),
        DELETEDCARD("Клиент %s отказался от карты №%s"),
        NEWLOAN("Клиент %s взял кредит на сумму %.2f"),
        NEWDEPOSIT("Клиент %s открыл депозит на сумму %.2f"),
        GAMEOVER("ЦБ: отзыв лицензии у банка %s. \nОтрицательный капитал: %.2f рублей \nGAME OVER!"),
        NOCLIENTS("Нет клиентов!"),
        ILLEGALARGUMENT("Операция невозможна: %s");
        private String text;

        Messages(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    private final String[] lastNames = {"Иванов", "Петров", "Сидоров", "Морозов", "Смирнов", "Попов", "Кузнецов", "Козлов", "Бобров", "Баранов"};
    private final String[] firstNames = {"Иван", "Петр", "Александр", "Алексей", "Андрей", "Виктор", "Кирилл", "Ярослав", "Роман", "Степан", "Илья"};
    private final String[] companyName = {"Рога и Копыта", "Вектор", "Ромашка", "Эдельвейс", "Главпродукт", "Снежинка", "Альянс", "Сладкий рай"};
    private Bank bank;

    public void run() {
        bank = new Bank("Сбербанк", 100_000_000);
        bank.addClient("Гендиректор", "666", ClientType.NATURAL);
        do {
            Operations operation = getRandomOperation();
            try {
                switch (operation) {
                    case ADDCLIENT:
                        addClient();
                        break;
                    case DELETECLIENT:
                        deleteClient();
                        break;
                    case ISSUECARD:
                        issueCard();
                        break;
                    case BLOCKCARD:
                        blockCard();
                        break;
                    case ISSUELOAN:
                        issueLoan();
                        break;
                    case CREATEDEPOSIT:
                        createDeposit();
                        break;
                }
            } catch (NoSuchElementException ex) {
                System.out.println(Messages.NOCLIENTS.getText());

            } catch (IllegalArgumentException ex) {
                System.out.println(String.format(Messages.ILLEGALARGUMENT.getText(), ex.getMessage()));
            }
        } while (bank.calculateBalance() > 0);
        System.out.println(String.format(Messages.GAMEOVER.getText(), bank.getBankName(), bank.calculateBalance()));
    }

    private void createDeposit() {
        int clientID = getRandomClientID();
        double depositSum = getRandomInt(100_000);
        System.out.println(String.format(Messages.NEWDEPOSIT.getText(), bank.getClient(clientID).getFullName(), depositSum));
    }

    private void issueLoan() {
        int clientID = getRandomClientID();
        double loanSum = getRandomInt(10_000_000);
        bank.issueLoan(clientID, loanSum);
        System.out.println(String.format(Messages.NEWLOAN.getText(), bank.getClient(clientID).getFullName(), loanSum));
    }

    private void addClient() {
        ClientType clientType = getRandomClientType();
        int clientID = bank.addClient(generateFullName(clientType), getRandomUniqueIdent(), clientType);
        Client client = bank.getClient(clientID);
        System.out.println(String.format(Messages.NEWCLIENT.getText(), client.getFullName(), client.getClientTypeName()));
    }

    private void deleteClient() {
        int clientID = getRandomClientID();
        Client client = bank.getClient(clientID);
        System.out.println(String.format(Messages.DELETEDCLIENT.getText(), client.getFullName(), client.getTotalBalance()));
        bank.deleteClient(clientID);
    }

    private void issueCard() {
        int clientID = getRandomClientID();
        String cardNumber = bank.issueCard(clientID, getRandomCardType());
        System.out.println(String.format(Messages.NEWCARD.getText(), bank.getClient(clientID).getFullName(), cardNumber));
    }

    private void blockCard() {
        String cardNumber = bank.getCards().entrySet().iterator().next().getKey();
        int clientID = bank.getClientByCardNumber(cardNumber);
        System.out.println(String.format(Messages.DELETEDCARD.getText(), bank.getClient(clientID).getFullName(), cardNumber));
        bank.blockCard(clientID, cardNumber);
    }

    private String generateFullName(ClientType client) {
        switch (client) {
            case NATURAL:
                return String.format("%s %s", getRandomName(lastNames), getRandomName(firstNames));
            case JURIDICAL:
                return getRandomName(companyName);
            default:
                return "";
        }
    }

    private String getRandomName(String[] strArray) {
        return strArray[getRandomInt(strArray.length)];
    }

    private Operations getRandomOperation() {
        return Operations.values()[getRandomInt(Operations.values().length)];
    }

    private ClientType getRandomClientType() {
        return ClientType.values()[getRandomInt(ClientType.values().length)];
    }

    private AccountType getRandomCardType() {
        if (getRandomInt(2) > 0) {
            return AccountType.CREDITCARD;
        } else {
            return AccountType.DEBITCARD;
        }
    }

    private String getRandomUniqueIdent() {
        return String.format("010%d", getRandomInt(Integer.MAX_VALUE));
    }

    private int getRandomClientID() throws NoSuchElementException {
        int clientID = 0;
        for (Map.Entry<Integer, Client> entry : bank.getClients().entrySet()) {
            int random = getRandomInt(Integer.MAX_VALUE);
            clientID = entry.getKey();
            if (random < entry.getKey()) {
                break;
            }
        }
        return clientID;
    }

    private int getRandomInt(int maxValue) {
        return ThreadLocalRandom.current().nextInt(0, maxValue);
    }


}
