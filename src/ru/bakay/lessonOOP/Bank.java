package ru.bakay.lessonOOP;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class Bank implements IBank {

    public Bank(String bankName, double authorisedCapital) {
        this.bankName = bankName;
        this.authorisedCapital = authorisedCapital;
        clientHashMap = new HashMap<>();
        cardHashMap = new HashMap<>();
    }

    private enum Messages {
        DBOVERSIZE("Превышен размер базы"),
        INVALIDCLIENTID("Клиент не найден"),
        ACCOUNTNOTFOUND("Счет не найден");
        private String text;

        Messages(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    enum ClientIDValues {
        MIN_VALUE(1234),
        MAX_VALUE(Integer.MAX_VALUE),
        MAX_SIZE(Integer.MAX_VALUE - 500_000_000);

        private final int value;

        ClientIDValues(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private String bankName;
    private double authorisedCapital;
    private Map<Integer, Client> clientHashMap;
    private Map<String, Integer> cardHashMap;

    public String getBankName() {
        return bankName;
    }

    public double getAuthorisedCapital() {
        return authorisedCapital;
    }

    @Override
    public String issueCard(int clientID, AccountType cardType) {
        Card card = (Card) (createAccount(clientID, cardType, 0));
        cardHashMap.put(card.getCardNumber(), clientID);
        return card.getCardNumber();
    }

    public int getClientByCardNumber(String cardNumber) {
        return cardHashMap.get(cardNumber);
    }

    public Account getAccountByCardNumber(String cardNumber) throws IllegalArgumentException {
        Account foundAccount = null;
        List<Account> accountList = clientHashMap.get(getClientByCardNumber(cardNumber)).getAccountList();
        for (Account account : accountList) {
            if ((account instanceof Card) && (((Card) account).getCardNumber().equals(cardNumber))) {
                foundAccount = account;
            }
        }
        if (foundAccount == null) {
            throw new IllegalArgumentException(Messages.ACCOUNTNOTFOUND.getText());
        } else {
            return foundAccount;
        }
    }

    public Map<String, Integer> getCards() {
        return cardHashMap;
    }

    public Map<Integer, Client> getClients() {
        return clientHashMap;
    }

    @Override
    public void blockCard(int clientID, String cardNumber) {
        clientHashMap.get(clientID).getAccountList().remove(getAccountByCardNumber(cardNumber));
        cardHashMap.remove(cardNumber);
    }

    @Override
    public int addClient(String fullName, String uniqueIdent, ClientType clientType) throws IllegalArgumentException {
        int clientID;

        if (clientHashMap.keySet().size() >= ClientIDValues.MAX_SIZE.getValue()) {
            throw new IllegalArgumentException(Messages.DBOVERSIZE.getText());
        }
        do {
            clientID = ThreadLocalRandom.current().nextInt(ClientIDValues.MIN_VALUE.getValue(), ClientIDValues.MAX_VALUE.getValue());
        } while (clientHashMap.containsKey(clientID));

        Client client = null;
        switch (clientType) {
            case JURIDICAL:
                client = new JuridicalPerson(fullName, uniqueIdent);
                break;
            case NATURAL:
                client = new NaturalPerson(fullName, uniqueIdent);
                break;
        }
        clientHashMap.put(clientID, client);
        return clientID;
    }

    @Override
    public void deleteClient(int clientID) {
        clientHashMap.remove(clientID);
    }

    @Override
    public Client getClient(int clientID) throws IllegalArgumentException {
        if (hasClient(clientID)) {
            return clientHashMap.get(clientID);
        } else {
            throw new IllegalArgumentException(Messages.INVALIDCLIENTID.getText());
        }
    }

    public boolean hasClient(int clientID) {
        return clientHashMap.containsKey(clientID);
    }

    @Override
    public void issueLoan(int clientID, double loanSum) {
        createAccount(clientID, AccountType.LOAN, loanSum);
    }

    @Override
    public void createDeposit(int clientID, double depositSum) {
        createAccount(clientID, AccountType.DEPOSIT, depositSum);
    }

    @Override
    public double calculateBalance() {
        double balance = authorisedCapital;
        for (Map.Entry<Integer, Client> entry : clientHashMap.entrySet()) {
            balance += entry.getValue().getTotalBalance();
        }
        return balance;
    }

    private Account createAccount(int clientID, AccountType accountType, double sum) {
        List<Account> accountList;
        accountList = getClient(clientID).getAccountList();
        Account account = null;
        switch (accountType) {
            case DEPOSIT:
            case LOAN:
                account = new Account(accountType, sum);
                break;
            case DEBITCARD:
            case CREDITCARD:
                account = new Card(accountType, sum, accountList.size() + 1);
                break;
        }
        accountList.add(account);
        return account;
    }
}
