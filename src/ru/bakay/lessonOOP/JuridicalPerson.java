package ru.bakay.lessonOOP;

public class JuridicalPerson extends Client {
    private String INN;

    public String getINN() {
        return INN;
    }

    public JuridicalPerson(String companyName, String INN) {
        super(companyName, ClientType.JURIDICAL);
        this.INN = INN;
    }
}
