package ru.bakay.lessonOOP;

public enum  ClientType {
    JURIDICAL("юридическое лицо"),
    NATURAL("физическое лицо");

    private String title;

    ClientType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
