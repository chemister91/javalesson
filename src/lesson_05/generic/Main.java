package lesson_05.generic;


import lesson_05.generic.box.IntegerBox;
import lesson_05.generic.box.LongBox;
import lesson_05.generic.box.StringBox;
import lesson_05.generic.generic.GenericBox;

public class Main {
    public static void main(String[] args) {
        IntegerBox integerBox = new IntegerBox(5);
        integerBox.setValue(10);
        System.out.println(integerBox.getValue());

        LongBox longBox = new LongBox(5L);
        longBox.setValue(10L);
        System.out.println(longBox.getValue());

        StringBox stringBox = new StringBox("Значение1");
        stringBox.setValue("Значение 2");
        System.out.println(stringBox.getValue());

        GenericBox<Integer> genericBox1= new  GenericBox<Integer>(5);
        //Пример несоответствия типов
        //genericBox1 = new GenericBox<Long>(5L);
        genericBox1.setValue(10);
        System.out.println(genericBox1.getValue());

        GenericBox<?> genericBox3;

        genericBox3 = new  GenericBox<Integer>(5);
        //Пример несоответствия типов
        //genericBox1 = new GenericBox<Long>(5L);
        genericBox1.setValue(10);
        System.out.println(genericBox1.getValue());

        //Diamond Operator
        GenericBox<Long> genericBox2= new  GenericBox<>(5L);
        //Пример несоответствия типов
        //genericBox1 = new GenericBox<Long>(5L);
        genericBox2.setValue(10L);
        System.out.println(genericBox2.getValue());

        //ClassCastException:
        //ObjectBox objectBox = new ObjectBox(new Integer (5));
        //objectBox.setValue("SomeStringValue");
        //String message = (String) objectBox.getValue();

        System.out.println(method("5"));
        System.out.println(methodExtends(5));

        //Пример
    }

    public static <T> T method (T value){
        return value;
    }


    public static <T extends Number> T methodExtends (T value){
        return value;
    }
}
