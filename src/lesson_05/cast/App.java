package lesson_05.cast;

/**
 * Created by arty on 18.12.2018.
 */
public class App {
    public static void main(String[] args) {
        Child c[] = new Child[5];
        Parent p[] = c;
        p[0] = new Parent();
        //process(new Child[3]);
    }

    public static void process(Parent[] p) {
        if (p != null && p.length > 0) {
            p[0] = new Parent();
        }
    }
}
